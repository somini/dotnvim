#!/usr/bin/env ruby
# encoding: utf-8

# Source: https://github.com/hyiltiz/vim-plugins-profile
# Copyright 2015-2017, Hörmet Yiltiz <hyiltiz@github.com>
# Released under GNU GPL version 3 or later.
# Modified by somini <dev@somini.xyz>

require 'tempfile'

# Users can pass "nvim" as a first argument to use vim.
vim_bin = ARGV.first || 'nvim'

LOG = Tempfile.new(["vim-plugins-profile", "log"])

XDG_CONFIG_HOME = ENV['XDG_CONFIG_HOME'] || File.join(ENV['HOME'], '.config')

VIMFILES_DIR = File.join(XDG_CONFIG_HOME, 'nvim')
puts "Assuming your vimfiles folder is #{VIMFILES_DIR}."

screen_width = ENV['SCREEN_WIDTH'] || %x[tput cols].strip
if screen_width =~ /^\d+$/
	screen_width = screen_width.to_i
else
	screen_width = 0
end
if screen_width.nil? || screen_width < 1
	screen_width = 80 # Sensible defaults if $TERM is not defined
end
puts "Screen Width: #{screen_width}"

puts "Generating #{vim_bin} startup profile..."
system(vim_bin, '--headless', '--startuptime', LOG.path, '+qa!')

LOG.unlink # https://ruby-doc.org/stdlib-2.4.2/libdoc/tempfile/rdoc/Tempfile.html#method-i-unlink-label-Unlink-before-close

begin
	plug_dirs = ARGV[1..-1] || ['1st-party', '3rd-party', 'plugin'].map { |f| File.join(VIMFILES_DIR, f) }
	puts "Plugin directories:"
	plug_dirs.each {|dir|
		puts "  #{dir}"
	}

	# parse
	exec_times_by_name = Hash.new(0)
	all_lines = LOG.readlines()
	lines = all_lines.select { |line|
		result = plug_dirs.select {|d|
			line =~ /sourcing .*#{Regexp.escape(d)}/
		}
		result != []
	}
	puts "#{lines.length} lines contain plugin sources"
	lines.each do |line|
		_, _, exec_time, _, path = line.split(' ')
		relative_path = path.gsub(VIMFILES_DIR + '/', '')
		plugin_name = relative_path.split('/')[0..1].join('/')
		time = exec_time.to_f
		exec_times_by_name[plugin_name] += time
	end
	puts "TOTAL Startup time: #{all_lines[-1].split(' ')[0].to_f.round(3)} ms"
	puts
	puts "# Plot #{'#' * (screen_width - 7)}"
	puts
	max = exec_times_by_name.values.max
	relatives = exec_times_by_name.reduce({}) do |hash, (name, time)|
		hash.merge!(name => time/max.to_f)
	end
	max_name_length = relatives.keys.map(&:length).max || 0
	time_length = 7
	# -4 :: "|" "ms|"
	plot_width = screen_width - max_name_length - time_length - 4
	Hash[ relatives.sort_by { |k, v| -v } ].each do |name, rel_time|
		time = exec_times_by_name[name]
		puts "#{name.rjust(max_name_length)}|#{time.round(3).to_s.ljust(time_length)}ms|#{'*' * (rel_time*plot_width)}"
	end
ensure
	LOG.close!
end
