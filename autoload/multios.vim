let s:cached_result = v:null

function! multios#detect() abort
	if s:cached_result is v:null
		" Detect OS
		let l:result = {
					\ 'os': v:null,
					\ 'bits': v:null,
					\ }
		" Shameless ripoff: https://vi.stackexchange.com/a/2577
		if has('win32') || has('win64') || has('win16')
			let l:result['os'] = 'Windows'
			if has('win64')
				let l:result['bits'] = 64
			elseif has('win32')
				let l:result['bits'] = 32
			elseif has('win16')
				let l:result['bits'] = 16
			endif
		elseif has('unix') "All the unices, MacOS X included
			" Just use `uname -mo`
			if executable('uname')
				let l:output = systemlist(['uname', '-mo'])
				let l:values = split(l:output[0], ' ')
				let l:result['os'] = l:values[1]
				let l:result['bits'] = get({
							\ 'x86_64': 64,
							\ 'i386': 32,
							\ }, l:values[0], v:null)
			endif
		else
			finish " Unsupported, I can't make any guarantees
		endif

		" Save cached value
		let s:cached_result = l:result
	endif
	return s:cached_result
endfunction

function! multios#redetect() abort
	let s:cached_result = v:null
	return multios#detect()
endfunction

function! multios#os() abort
	return multios#detect()['os']
endfunction

function! multios#bits() abort
	return multios#detect()['bits']
endfunction

function! multios#init() abort
	let g:os = multios#os()
	let g:bits = multios#bits()

	let l:runtime_paths = []
	if g:os isnot v:null
		" Turn 'GNU/Linux' into 'GNU-Linux'
		let l:os_name = substitute(g:os, '\V/', '-', 'g')

		" Load OS-specific `init.vim`
		let l:os_dir = printf('multios/%s', l:os_name)
		execute 'runtime!' printf('%s/init.vim', l:os_dir)

		call add(l:runtime_paths, l:os_dir)
	endif

	if g:bits isnot v:null
		" Load the OS and bits-specific `init.vim`
		let l:osbits_dir = printf('multios/%s.%s', l:os_name, g:bits)
		execute 'runtime!' printf('%s/init.vim', l:osbits_dir)

		call add(l:runtime_paths, l:osbits_dir)
	endif

	" Setup runtime path directories
	for l:dir in l:runtime_paths
		if isdirectory(l:dir)
			call pathogen#surround(fnamemodify(l:dir, ':p'))
		endif
	endfor
endfunction
