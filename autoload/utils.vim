function! utils#CheckDir(dir) abort
	if !isdirectory(a:dir)
		echom 'Making directory: ' . a:dir
		call mkdir(a:dir, 'p')
	endif
	return a:dir
endfunction

function! utils#IsLocalPath(path) abort
	" Reject "protocol://"
	return a:path !~# '^\w\+:\/\/.*'
endfunction
