---
# Use the FreeDesktop CI Templates
# https://freedesktop.pages.freedesktop.org/ci-templates/templates.html
# Current Master: https://gitlab.freedesktop.org/freedesktop/ci-templates/-/commits/master
include:
  - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/3e66ea37e5672bb8f48e3056ba92915b5fc5b888/templates/arch.yml'
  - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/3e66ea37e5672bb8f48e3056ba92915b5fc5b888/templates/centos.yml'
  - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/3e66ea37e5672bb8f48e3056ba92915b5fc5b888/templates/ci-fairy.yml'

default:
  interruptible: true

variables:
  fdo_current_tag: '2022-01-04'

workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH'

stages:
  - prepare
  - container
  - build
  - test
  - archives
  - publish

.info:arch:
  variables:
    FDO_DISTRIBUTION_TAG: "${fdo_current_tag}"

.info:centos:
  variables:
    FDO_DISTRIBUTION_VERSION: 8
    FDO_DISTRIBUTION_TAG: "${fdo_current_tag}"

.info:centos-oob:
  variables:
    FDO_DISTRIBUTION_VERSION: 8
    FDO_DISTRIBUTION_TAG: "${fdo_current_tag}-oob"

.container:arch:
  extends:
    - .fdo.distribution-image@arch
    - .info:arch

.container:centos:
  extends:
    - .fdo.distribution-image@centos
    - .info:centos

.container:centos-oob:
  extends:
    - .fdo.distribution-image@centos
    - .info:centos-oob

.fetch-container:
  stage: prepare
  needs: []
  # An image with "skopeo"
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/buildah:2021-03-18.1
  variables:
    UPSTREAM_REGISTRY: registry.gitlab.com
    UPSTREAM_PROJECT_PATH_SLUG: somini/dotnvim
  script:
    # Fetch the upstream images and push them to this registry
    - skopeo login --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - >
      skopeo copy
      "docker://$UPSTREAM_REGISTRY/$UPSTREAM_PROJECT_PATH_SLUG/$DISTRO/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG"
      "docker://$CI_REGISTRY/$CI_PROJECT_PATH_SLUG/$DISTRO/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG"
  rules:
    # Only do t automatically outside the upstream repo
    - if: '$CI_REGISTRY == $UPSTREAM_REGISTRY'
      when: manual
      allow_failure: true

fetch-containers:arch:
  extends:
    - .info:arch
    - .fetch-container
  variables:
    DISTRO: arch

fetch-containers:centos:
  extends:
    - .info:centos
    - .fetch-container
  variables:
    DISTRO: centos

fetch-containers:centos-oob:
  extends:
    - .info:centos-oob
    - .fetch-container
  variables:
    DISTRO: centos-oob

build-container:arch:
  stage: container
  needs:
    - job: fetch-containers:arch
      optional: true
  extends:
    - .fdo.container-build@arch
    - .info:arch
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'git make ripgrep exa fd zip neovim python python-pip python-neovim vint'

build-container:centos:
  stage: container
  needs:
    - job: fetch-containers:centos
      optional: true
  extends:
    - .fdo.container-build@centos
    - .info:centos
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'git make'
    FDO_DISTRIBUTION_EXEC: "build/postinstall-centos.sh appimage-extract"

build-container:centos-oob:
  stage: container
  needs:
    - job: fetch-containers:centos-oob
      optional: true
  extends:
    - .fdo.container-build@centos
    - .info:centos-oob
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'git make'
    FDO_DISTRIBUTION_EXEC: "build/postinstall-centos.sh oob"

dist:
  extends: .container:arch
  stage: build
  needs:
    - build-container:arch
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  script:
    - make dist/Full dist/Single
  cache:
    key: spell
    paths:
      - spell/data
  artifacts:
    name: "${CI_COMMIT_REF_NAME}"
    expire_in: "1 mos"  # 1 months
    paths:
      - dist
      - spell

check:
  extends: .container:arch
  stage: test
  needs:
    - build-container:arch
    - dist
  script:
    - make check

.nvim-sanity: &nvim-sanity
  stage: test
  script:
    - make nvim-sanity
  artifacts:
    paths:
      - dist/test

nvim-sanity:arch:
  extends: .container:arch
  <<: *nvim-sanity
  needs:
    - build-container:arch
    - dist

nvim-sanity:centos:
  extends: .container:centos
  <<: *nvim-sanity
  needs:
    - build-container:centos
    - dist

nvim-sanity:centos-oob:
  extends: .container:centos-oob
  <<: *nvim-sanity
  needs:
    - build-container:centos-oob
    - dist
  allow_failure: true  # Just a warning

lint-vint:
  extends: .container:arch
  needs:
    - build-container:arch
  stage: test
  script:
    - make lint

archives:
  extends: .container:arch
  stage: archives
  needs:
    - build-container:arch
    - dist
  script:
    - make -j dist/{Full,Single}.{tar.gz,zip}
    - make html-pages
  artifacts:
    name: "${CI_COMMIT_REF_NAME}"
    expire_in: "1 mos"  # 1 months
    paths:
      - dist/*.html
      - dist/*.bash
      - dist/*.zip
      - dist/*.tar.gz

pages:
  extends: .container:arch
  stage: publish
  needs:
    - build-container:arch
    - archives
  script:
    # Make sure there's no "public" directory
    - test -d public && exit 1
    - mv dist public
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
