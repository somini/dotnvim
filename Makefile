VINT := vint

TARGET := dist

.PHONY: spell
spell:
	$(MAKE) -C $@

dist:
	mkdir $@

# Sentinel-ish file
$(TARGET)/Full/VERSION: $(TARGET)/Full/
$(TARGET)/Full: dist spell
	bin/dist-full $@

# Sentinel-ish file
$(TARGET)/Single/VERSION: $(TARGET)/Single/
$(TARGET)/Single: $(TARGET)/Full/VERSION
	bin/dist-single $(dir $<) $@

ARCHIVE_FOLDER := nvim
# Tar.gz
$(TARGET)/%.tar.gz: $(TARGET)/%/VERSION
	tar --create --file $@ --auto-compress --sort=name --transform='s@^$(patsubst %/,%,$(dir $<))@$(ARCHIVE_FOLDER)@' $(dir $<)
# Zip
$(TARGET)/%.zip: $(TARGET)/zip/%/$(ARCHIVE_FOLDER) $(TARGET)/%/VERSION
	cd $(dir $<) && rg --sort path --files $(ARCHIVE_FOLDER) | zip $(notdir $@) -@
	mv --no-target-directory $(dir $<)$(notdir $@) $@

$(TARGET)/%.html: build/%.html | dist
	build/generate-index <$< >$@

$(TARGET)/%.bash: build/%.bash | dist
	build/generate-index <$< >$@
	chmod +x $@

.PHONY: html-pages
html-pages: $(TARGET)/index.html $(TARGET)/install-git.bash $(TARGET)/install.bash

# Profile-specific symlinks
$(TARGET)/test/%/nvim $(TARGET)/zip/%/$(ARCHIVE_FOLDER): $(TARGET)/%/VERSION
	mkdir -p $(dir $@)
	ln --symbolic --relative --no-target-directory $(TARGET)/$* $@

.PHONY: check nvim-profile nvim-sanity lint

check: $(TARGET)/Full/VERSION
	# Full
	@bin/dist-delta $(dir $<)

nvim-sanity: $(TARGET)/test/Full/nvim.processed.err $(TARGET)/test/Single/nvim.processed.err
	@cat $^ || true
	build/test-sanity $^

nvim-profile: $(TARGET)/test/Full/nvim.profile.log $(TARGET)/test/Single/nvim.profile.log
	@cat $^

lint:
	rg --files -0 --type=vim -g '!1st-party' -g '!3rd-party' | xargs -0 -- $(VINT) --color --stat

$(TARGET)/test/%/nvim.processed.err: $(TARGET)/test/%/nvim
	# Open nvim in headless mode and exit
	XDG_CONFIG_HOME=$(dir $<) nvim --headless +q >$(dir $@)nvim.out 2>$(dir $@)nvim.err
	# Standard Output
	@cat $(dir $@)nvim.out ; echo
	# Standard Error
	@cat $(dir $@)nvim.err ; echo
	# Filter error output for known non-error messages
	# - Automatic Folder creation (`utils#CheckDir`)
	@<$(dir $@)nvim.err | grep -vE \
		-e '^Making directory: $(HOME)' \
		>$@ || true # Don't use the grep exit value

$(TARGET)/test/%/nvim.profile.log: $(TARGET)/test/%/nvim
	XDG_CONFIG_HOME=$(dir $<) bin/plugins-profile.rb >$@

.PHONY: clean clean-archives clean-test

clean:
	$(RM) -r $(TARGET)

clean-archives:
	$(RM) -r $(TARGET)/zip

clean-test:
	$(RM) -r $(TARGET)/test
