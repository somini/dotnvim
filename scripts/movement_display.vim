" Move vertically by display lines
if maparg('j', 'n') ==# '' "Don't override
	noremap <buffer> j gj
endif
noremap <buffer> gj j
if maparg('k', 'n') ==# '' "Don't override
	noremap <buffer> k gk
endif
noremap <buffer> gk k

" Move horizontally by display lines
noremap <buffer> $ g$
noremap <buffer> g$ $
noremap <buffer> 0 g0
noremap <buffer> g0 0
