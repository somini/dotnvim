#!/usr/bin/bash
# Install the Development Version of dotnvim
set -e

location="${1:-"$HOME/.config/nvim"}"
echo "Location: '$location'"

if [ -d "$location" ]; then
	echo "- Location not empty!"
	exit 1
fi
# Clone Repository
git clone --recursive "##PROJECT_URL##" "$location"
# Initial Setup of nvim repository
"$location/bin/nvim-reload"
# Setup the spellchecking data
make -C "$location/spell"
