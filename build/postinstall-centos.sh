#!/bin/sh
set -e

where='/usr/local/bin/nvim'
how='oob'

nvim_appimage='https://github.com/neovim/neovim/releases/latest/download/nvim.appimage'

usage() {
	cat >&2 <<-HELP
	$(basename "$0") [-H oob|appimage|appimage-extract] [-W LOCATION]
	Install neovim on CentOS

	-H: How?
	    - oob: Out-of-the-Box. Uses EPEL
	    - appimage: Use the latest AppImage
	    - appimage-extract: Use the latest AppImage, extracted
	    Defaults to '$how'
	-W: Where to?
	    When AppImage needs to be extracted, it will be included on the
	    same location, with a '.d' suffix.
	    Defaults to '$where'
	HELP
	exit 100
}

while getopts ':H:W:' OPT; do
	case "$OPT" in
		W) where="$OPTARG" ;;
		H) how="$OPTARG" ;;
		*) usage;;
	esac
done

if [ -f "$where" ]; then
	echo 'ERROR: LOCATION exists' >&2
	usage
fi

# Setup neovim
if [ "$how" = "oob" ]; then
	# Setup EPEL
	dnf install -y epel-release
	#
	dnf install -y neovim
elif [ "$how" = "appimage" ]; then
	# - AppImage (FUSE)
	curl -L "$nvim_appimage" >"$where"
	chmod +x "$where"
elif [ "$how" = "appimage-extract" ]; then
	# - AppImage (extracted)
	# TODO: This is only necessary if `appimage` fails.
	cd "$(mktemp -d)"
	dnvim="./nvim.appimage"
	fnvim="${where}.d"
	curl -L "$nvim_appimage" >"$dnvim"
	chmod +x "$dnvim"
	"$dnvim" --appimage-extract
	mv -vT "squashfs-root" "$fnvim"
	ln -svT "$fnvim/AppRun" "$where"
	cd -
	rm -rf "$OLDPWD"
else
	echo "=> Invalid How: '$1'"
	exit 100
fi
# Setup neovim - Python 3 interface
dnf install -y epel-release python3-neovim

# Check the neovim version
nvim --version
