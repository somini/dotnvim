#!/usr/bin/bash
# Install or Update the Standalone Version of dotnvim
set -e

location="${1:-"$HOME/.config"}"
edition="${2:-"Full"}"
echo "Location: '$location'"
echo "Edition: $edition"

if ! [ -d "$location" ]; then
	mkdir -v "$location"
fi

new_location="$location/nvim"
old_location="$new_location-$(date -Iseconds)"
version_file="$new_location/VERSION"
if [ -d "$new_location" ]; then
	echo "- Install Location not empty!"
	if [ ! -d "$old_location" ]; then
		echo "- Moving existing installation to '$old_location'"
		if [ -f "$version_file" ]; then
			echo "  - Old VERSION: $(head -1 "$version_file")"
		fi
		mv -vT "$new_location" "$old_location"
	else
		exit 1
	fi
fi

echo '# Unpack the tarball'
curl -L "##URL##/$edition.tar.gz" | tar xzf - -C "$location"
echo '# Initial Setup of nvim repository'
if "$new_location/bin/nvim-reload"; then
	echo '# All Good!'
	echo "  - New VERSION: $(head -1 "$version_file")"
else
	if [ -d "$old_location" ]; then
		echo "- Error, restoring old installation"
		rm -rf "$new_location"
		mv -vT "$old_location" "$new_location"
	fi
	exit 2
fi
