" somini.nvimrc

" Pathogen {{{
scriptencoding utf-8
if !exists('g:init_environment')
	if !empty(globpath(&runtimepath, 'autoload/pathogen.vim', v:true, v:true))
		"Pathogen in runtime, this is a production environment
		let g:init_environment = 'production'
		runtime autoload/pathogen.vim
	else
		"Pathogen MIA, this is development environment
		let g:init_environment = 'development'
		runtime 3rd-party/pathogen/autoload/pathogen.vim
		if !exists('*pathogen#infect')
			echomsg "Can't load pathogen, won't change any options!"
			colorscheme blue "Ugly-ass colors to hammer the point home
			finish
		endif
		" Plugin Blacklist {{{
		" The big one, commited into git and the host-specific one
		let s:plugin_blacklist = expand(expand('<sfile>:p:h').'/plugin_blacklist')
		" The host-specific one is defined as a function of the big one
		let s:plugin_blacklist_host = s:plugin_blacklist . '_host'
		let s:blacklist = []
		for s:f in [s:plugin_blacklist, s:plugin_blacklist_host]
			if filereadable(s:f)
				let s:blacklist += readfile(s:f)
			endif
		endfor
		unlet s:f
		if !empty(s:blacklist)
			let g:pathogen_blacklist = s:blacklist
		endif
		"}}}
		execute pathogen#infect('1st-party/{}', '3rd-party/{}')
	endif
endif
"}}}

" Generic Configuration {{{
" Load the multi-OS configuration
call multios#init()

let s:helper_program = ''
if executable('rg')
	let s:helper_program = 'ripgrep'
elseif executable('ag')
	let s:helper_program = 'ag'
endif

set report=0 "Report changes in any number of lines, not just >2
set updatetime=500 "ms
" Timeouts and stuff, Esc is instantaneous but can also be used in mappings
set notimeout
set ttimeout
set ttimeoutlen=100 "ms
augroup vimrc_legacy | autocmd!
	autocmd VimEnter * call s:SetupLegacy()
augroup END
function! s:SetupLegacy()
	" Matchit | Fixed on v0.4
	if !has('nvim-0.4')
		sunmap %
		sunmap a%
		sunmap g%
		sunmap [%
		sunmap ]%
	endif
endfunction
set tagcase=followscs " https://github.com/vim/vim/issues/712

let s:fenced_languages = ['html', 'python', 'ruby', 'rust', 'bash=sh']

" This was the default before...
set mouse=a
"}}}

" Utilities {{{
" Command line configuration
" Complete the longest common prefix, then use <Tab> to cycle
" through the various matches.
set wildmenu
set wildmode=longest:full,list:full
set wildignorecase " Ignore case
set wildoptions-=pum

" Insert completion configuration
" Keep the same-ish configuration from the command line
" The SuperTab plugin helps with the rest
set complete=.,w,b,u,U,t,i
set completeopt=menu,preview,longest
set isfname-== " Remove = from filenames. Breaks files with = on the name

set lazyredraw "Don't update the screen in the middle of macros, etc
" More information with less clicks
nnoremap <C-g> 2<C-g>
set nojoinspaces "TODO: Should only be activated on "text" buffers
" Keep the cursor still when joining lines
nnoremap J m`J``

" Number Formats to be considered by
" *CTRL-a*/*v_g_CTRL-A*/*CTRL-x*/*v_g_CTRL-X*
set nrformats=alpha,hex,bin

" Setup the leader keys
let g:mapleader = '\'
let g:maplocalleader = ','
" Root Markers {{{
" Account for git submodules
let g:root_marker_manual = '.RootMarker'
let g:root_markers_vcs = [
			\ '.git',
			\ '.git/',
			\ '.hg/',
			\ '.svn/',
			\ '.bzr/',
			\ '_darcs/',
			\]
let g:root_markers_other = [
			\ 'Makefile',
			\ 'Rakefile',
			\ 'setup.py', 'requirements.txt',
			\]
let g:root_markers = [g:root_marker_manual] + g:root_markers_vcs + g:root_markers_other
"}}}
" Change Indentation {{{
nnoremap <silent> <Leader>ci :call <SID>stab()<CR>
function! s:stab()
	let l:in = input('Indentation Value: ')
	if !empty(l:in) && l:in =~# '\v[\+\-]?\d*'
		" Do it!
		call s:stab_change(l:in, 0)
	else
		echohl WarningMsg
		echo 'Pattern not a match'
		echohl None
		call s:stab_check()
	endif
endfunction
function! s:stab_change(val, check)
	let l:tabs = a:val =~# '^-' "NoExpandTab, unless you say so
	let l:value = abs(a:val)
	let &l:expandtab = l:tabs
	let &l:tabstop = l:value
	let &l:shiftwidth = l:value
	" " Refresh the indent guides
	" IndentGuidesToggle
	" IndentGuidesToggle
	if a:check
		call s:stab_check()
	endif
endfunction
function! s:stab_check()
	setlocal expandtab? tabstop? shiftwidth?
endfunction
function! s:stab_command(...)
	if a:0 == 1
		call s:stab_change(a:000[0], 1)
	else
		call s:stab_check()
	endif
endfunction
command! -nargs=? Stab call s:stab_command(<args>)
"}}}

" Edit a register on the command line {{{
function! s:ChangeRegister()
	let l:reg_regex = '[a-z]'
	echohl Question
	echo 'Choose register('.l:reg_regex.'): '
	echohl None
	let l:c = nr2char(getchar())
	if l:c =~? l:reg_regex
		"vint: -ProhibitUnnecessaryDoubleQuote
		let l:reg = tolower(l:c)
		return ":let @".l:reg." = \<C-r>=string(getreg(".string(l:reg)."))\<CR>\<Left>"
		"vint: +ProhibitUnnecessaryDoubleQuote
	else
		echohl WarningMsg
		echo 'Register invalid: "'.strtrans(l:c).'"'
		echohl None
	endif
endfunction
nnoremap <expr> <Leader>r <SID>ChangeRegister()
"}}}

" Clear search highlight
nnoremap <silent> <Plug>(vimrc-clear-hls) :nohlsearch<Bar>redraw<CR>
"}}}

" Data Safety and Managing {{{
nnoremap <silent> <Leader>w :call <SID>WriteCurrentBuffer()<CR>
function! s:WriteCurrentBuffer()
	" Reject readonly and `nowrite` buffers
	if &l:readonly || &l:buftype =~# 'nowrite'
		return
	endif
	let l:dir = expand('%:h:p')
	if utils#IsLocalPath(l:dir)
		call utils#CheckDir(l:dir)
	endif
	write
endfunction
" ALL the formats, by this order
" Existing order determined by the OS-specific default
for s:ff in ['unix', 'dos', 'mac']
	execute 'set' 'fileformats+='.s:ff
endfor
unlet s:ff

" ShaDa (AKA viminfo) {{{
set shada+='500 "Save marks for this much files
set shada+=f0 "Don't save '0-'9,'A-'Z
set shada+=! "Save uppercase variables
set shada+=h "Ignore search highlight
set shada+=s100 "Ignore registers >size in Kb
set shada+=<100 "Ignore registers >size in lines
"}}}
" Undo {{{
set undofile
set undolevels=5000 " Undo forever
if has('nvim-0.5')
	" New format in https://github.com/neovim/neovim/pull/13973 (f42e932, 2021-04-13).
	let &undodir = &undodir . '-0.5'
endif
"}}}
" Views {{{
set viewoptions+=slash " The One True Path Separator
set viewoptions+=unix  " The One True Format
"}}}
" Backup Files {{{
set backup writebackup "Backup and delete old file
set backupcopy=yes " Keep the inodes on Linux
set backupdir-=. "Don't use the current directory for backup
set backupext=.vim.bak
call utils#CheckDir(&backupdir) "Make sure the default directory is created
"}}}
" Sessions {{{
set sessionoptions-=help    "Don't store the help windows
set sessionoptions+=slash   "The One True Path Separator
set sessionoptions+=unix    "The One True Format
"}}}
" Cross-platform, leverage the nvim work
let s:user_temporary_dir = fnamemodify(&backupdir, ':h')
function! s:get_userdir(name)
	" `fnamemodify` should take care of Windows paths
	let l:dir = fnamemodify(s:user_temporary_dir.'/'.a:name, ':p')
	return utils#CheckDir(l:dir)
endfunction

set autoread " Automatically re-read files when they change outside vim
" autoread HARDER!
augroup vimrc_autoread | autocmd!
	autocmd FocusGained * checktime
augroup END

" Buffer Management {{{
function! s:vimrc_is_finalwindow()
	" Check if this is the final window on the final tab
	return tabpagenr('$') == 1 && len(tabpagebuflist(1)) == 1
endfunction
function! s:vimrc_onotherwindows()
	" Check if there are other windows into the same buffer
	if len(win_findbuf(winbufnr(0))) > 1
		return v:false
	else
		" Final window into buffer
		if s:vimrc_is_finalwindow()
			return v:false
		else
			return v:true
		endif
	endif
endfunction
function! s:vimrc_currentbuffer_kill()
	if s:vimrc_onotherwindows() == v:true
		bdelete!
	else
		quit!
	endif
endfunction
function! s:vimrc_currentbuffer_save()
	update
	if s:vimrc_onotherwindows() == v:true
		bdelete
	else
		xit
	endif
endfunction
command! ZQ call s:vimrc_currentbuffer_kill()
command! ZZ call s:vimrc_currentbuffer_save()
"}}}
nnoremap <silent> ZZ :ZZ<CR>
nnoremap <silent> ZQ :ZQ<CR>
"}}}

" Appearance {{{
let g:onedark_style = 'deep'
colorscheme onedark
set hlsearch "Highlight searches ...
nohlsearch "but not when starting up
set sidescroll=1 "Scroll 1 line at a time, horizontally
set scrolloff=5
set sidescrolloff=10
" Toggle line wrapping
nnoremap <silent> <Leader>tw :setlocal wrap!<CR>

" Nice unicode symbols
let s:paragraph_glyph = '¶'
let s:modified_glyph = '☢'
let s:unmodifiable_glyph = '⏚'
let s:empty_glyph = '¤'
let s:working_glyph = '⚒'
let s:global_glyph = '⌖'
let s:good_glyph = '✓'
let s:bad_glyph = '✗'
let s:type_glyph = {
			\ 'E': '✖',
			\ 'W': '‼',
			\ 'I': 'ℹ',
			\ 'M': '➤',
			\ }
let s:spell_glyph = '⁂'
let s:ellipsis_glyph = '⋯'
let s:strip_glyph = '§'
let s:checker_glyph = '⌘'
"TODO: 𝔾, 𝕊, etc.
let s:vcs_symbols = {
			\ 'git': 'G',
			\ 'svn': 'S',
			\ }

let s:glyph_sign_added = '+'
let s:glyph_sign_removed = '-'
let s:glyph_sign_removedfirst = s:glyph_sign_removed . '‾'
let s:glyph_sign_modified = '~'
let s:glyph_sign_modelete = s:glyph_sign_modified . s:glyph_sign_removed "Modified + Deleted

" Non-Printable Characters {{{
let &showbreak = ' ↪'
set list "Show non-printable characters
let &listchars = 'tab:  '
set listchars+=trail:•
set listchars+=precedes:⇇,extends:⇉
set listchars+=nbsp:·
function! s:ToggleList()
	let l:alttab = 'tab:≈~'
	if stridx(&listchars, l:alttab) == -1
		execute 'set' 'listchars+='.l:alttab
	else
		execute 'set' 'listchars-='.l:alttab
	endif
endfunction
nnoremap <silent> <Leader>tl :call <SID>ToggleList()<CR>
"}}}
" Numbers {{{
set numberwidth=1 " Minimal width of the number column
set nonumber norelativenumber " No line numbers by default ...
"... but toggle them with:
nnoremap <silent> <Leader>tn :call <SID>ToggleNumber()<CR>
nnoremap <silent> <Leader>tN :call <SID>ToggleRelativeNumber()<CR>
" Functions {{{
" Let the toggle number mapping toggle all numbers, and persist the relative
" number option with a script variable
function! s:ToggleNumber()
	if &number
		let s:vimrc_relativenumber = &relativenumber
		setlocal nonumber norelativenumber
	else
		setlocal number
		let &l:relativenumber = (exists('s:vimrc_relativenumber')) ? s:vimrc_relativenumber : 0 "OFF
	endif
endfunction
function! s:ToggleRelativeNumber()
	if &number
		setlocal relativenumber!
	else
		let s:vimrc_relativenumber = exists('s:vimrc_relativenumber') ? !s:vimrc_relativenumber : 1 "ON
	endif
endfunction
"}}}
"}}}
" Folds {{{
nnoremap <silent> <Leader>tf :call <SID>ToggleFoldsSidebar()<CR>
function! s:ToggleFoldsSidebar()
	let l:lvl = foldlevel('.')
	if l:lvl + 1 > &l:foldcolumn
		" Extent the column to the new distance
		let &l:foldcolumn = l:lvl + 1
	else
		if &l:foldcolumn > 0
			set foldcolumn=0
		else
			let &l:foldcolumn = foldlevel('.') + 1
		endif
	endif
endfunction
command! FoldsOpenAll call FoldsOpenAll()
function! FoldsOpenAll()
	try
		%foldopen!
	catch /^Vim\%((\a\+)\)\=:E490/
		" Do nothing for non-existent folds
	endtry
endfunction
"}}}
" Cursor Line and Column {{{
set nocursorline nocursorcolumn
nnoremap <silent> <Leader>tc :setlocal cursorline!<CR>
nnoremap <silent> <Leader>tC :setlocal cursorcolumn!<CR>
"}}}
" Tweak colorscheme {{{
augroup vimrc_appearance | autocmd!
	autocmd ColorScheme * call s:vimrc_postcolorscheme()
augroup END
function! s:vimrc_postcolorscheme()
	" Configure gutter sign colours
	highlight GutterSignAdd    guifg=#009900 ctermfg=2
	highlight GutterSignChange guifg=#bbbb00 ctermfg=3
	highlight GutterSignDelete guifg=#ff2222 ctermfg=1
endfunction
"}}}
"}}}

" Lightweight Vim-Easyclip {{{
" Inspired by:
" - https://github.com/svermeulen/vim-easyclip
" - https://github.com/nelstrom/vim-cutlass
set clipboard+=unnamedplus

" Delete: `d`
" Don't black-hole the text, use the "- register
noremap d "-d
sunmap d
nnoremap D "-D
nnoremap dd "-dd
nnoremap dD 0"-d$

" Cut: `x`
noremap x d
sunmap x
nnoremap xx dd
nnoremap X D

" Change: `c`
" Like delete + start insert mode
noremap c "-c
sunmap c
nnoremap cc "-S
nnoremap C "-C

" Substitute: `s`
"   Plugin: operator-replace
map s <Plug>(operator-replace)
" `ss` already works as expected
nmap sS m`0<Plug>(operator-replace)$``
nmap S <Plug>(operator-replace)$
sunmap s

" Paste: `p`
" Keep the regular semantics, the 'clipboard' setting should be enough
" Remove a legacy inconsistency
nmap Y y$
" <C-v> in Insert mode is muscle memory I can't drop, but keep the raw input
" functionality
" TODO: v:register
inoremap <C-v> <C-r>+
inoremap <C-q> <C-v>
"}}}

" Windowing Systems {{{
" Close all non-essential windows. Aliases closing only the preview.
nnoremap <silent> <C-w><C-z> :doautocmd User VimRC_CleanWindows<CR>
" General Cleanup, not just Windows
" See `:nohlsearch` for why repeating that here
nnoremap <silent> <Plug>(vimrc-cleanup) :doautocmd <nomodeline> User VimRC_Cleanup<CR>:nohlsearch <Bar> redraw<CR>
" Thorough cleanup
" General cleanup plus other tasks that take longer
nnoremap <silent> <Plug>(vimrc-thorough-cleanup) :doautocmd User VimRC_CleanupThorough<CR>

nmap <C-q> <Plug>(vimrc-cleanup)
nmap g<C-q> <C-q><Plug>(vimrc-thorough-cleanup)

augroup vimrc_cleanup | autocmd!
	" Close all non-essential windows:
	" - Quickfix
	autocmd User VimRC_CleanWindows cclose
	" - Location List
	autocmd User VimRC_CleanWindows lclose
	" - Preview
	autocmd User VimRC_CleanWindows pclose
	" - Help
	autocmd User VimRC_CleanWindows helpclose
	" - Fugitive Status Window (current tab)
	autocmd User VimRC_CleanWindows GstatusClose

	" On general cleanup:
	" - clean non-essential windows
	autocmd User VimRC_Cleanup doautocmd User VimRC_CleanWindows
	" - Reload the status line (sounds like a bug)
	autocmd User VimRC_Cleanup LightlineReload

	" Syntax from start
	autocmd User VimRC_CleanupThorough syntax sync fromstart
augroup END
"}}}

" Command Line {{{
" EMACS/Readline compatible (mostly)
" <C-b>: Move <1 character {{{
cnoremap <C-b> <Left>
cnoremap <C-x><C-b> <C-b>
"}}}
" <C-f>: Move >1 character {{{
cnoremap <C-f> <Right>
cnoremap <C-x><C-f> <C-f>
"}}}
" <A-b>: Move <1 word {{{
cnoremap <A-b> <C-Left>
"}}}
" <A-f>: Move >1 word {{{
cnoremap <A-f> <C-Right>
"}}}
" <C-a>: Start of Line {{{
cnoremap <C-a> <Home>
cnoremap <C-x><C-a> <C-a>
"}}}
"<C-e>: End of line
" <C-d>: Delete >1 character {{{
cnoremap <C-d> <C-r>=CommandLine_Helper_DeleteForward()<CR><Del>
cnoremap <C-x><C-d> <C-d>
"}}}
" <C-k>: Delete line >cursor {{{
cnoremap <C-k> <C-\>eCommandLine_KillForward()<CR>
cnoremap <C-x><C-k> <C-k>
"}}}
"<C-u>: Delete line <cursor
"<M-C-v>: Paste {{{
" TODO: v:register
cnoremap <A-C-v> <C-r>+
"}}}

" Helper Functions {{{
function! s:CommandLine_Is_End()
	return getcmdpos() - 1 == strlen(getcmdline())
endfunction
function! CommandLine_Helper_DeleteForward()
	" If the cursor is at the end of the command line,
	" insert a character to be deleted by the <Del>
	return s:CommandLine_Is_End() ? 'x' : ''
endfunction
function! CommandLine_KillForward()
	let l:line = getcmdline()
	let l:cur = getcmdpos()
	let l:s_from = 0
	let l:s_to   = l:cur - 1
	return strpart(l:line, l:s_from, l:s_to)
endfunction
"}}}
"}}}

" Terminal {{{
" Double Esc to leave terminal mode
tnoremap <Esc><Esc> <C-\><C-n>
" CTRL+Alt+v to paste
tnoremap <C-M-v> <C-\><C-n>pi

" Open a terminal in a horizontal/vertical split
" Takes a count meaning the window size
" Courtesy of: https://github.com/vimlab/split-term.vim
function! s:split_terminal(args, count, vertical, keep)
	" Open a terminal
	execute (a:count ? a:count : '').(a:vertical ? 'vnew' : 'new')
	if a:count
		" Keep window size constant
		setlocal winfixheight winfixwidth
	endif
	call termopen(a:args !=# '' ? a:args : $SHELL)
	if a:keep
		" Keep in the buffer: Start Insert Mode
		startinsert
	else
		" Leave the buffer:
		" - Get to the last line for automatic scrolling
		" - Move to last accessed window
		normal! G
		wincmd p
	endif
endfunction
command! -count -nargs=* -complete=shellcmd TermH call s:split_terminal(<q-args>, <count>, 0, 1)
command! -count -nargs=* -complete=shellcmd TermV call s:split_terminal(<q-args>, <count>, 1, 1)
command! -count -nargs=* -complete=shellcmd TermHLeave call s:split_terminal(<q-args>, <count>, 0, 0)
command! -count -nargs=* -complete=shellcmd TermVLeave call s:split_terminal(<q-args>, <count>, 1, 0)
"}}}

" Diff & Splits & Tabs {{{
" Diffs
if has('nvim-0.3.2')
	" Use the new integrated xdiff library, but support nvim v0.3.0
	set diffopt&
	set diffopt+=indent-heuristic
	set diffopt+=algorithm:patience
	set diffopt+=vertical
endif
" Jump to the next hunk, but choose how
nnoremap <silent> ]c :call <SID>HunkMoveTo(1)<CR>
nnoremap <silent> [c :call <SID>HunkMoveTo(0)<CR>
function! s:HunkMoveTo(next) "{{{
	let l:cmd = ''
	if &diff
		execute 'normal! '.(a:next ? ']c' : '[c')
		return
	endif
	if sy#buffer_is_active() && len(get(b:sy, 'hunks', [])) > 0
		" Use Signify
		let l:cmd = "\<Plug>".'(signify-'. (a:next ? 'next' : 'prev') .'-hunk)'
	elseif gitgutter#utility#is_active(bufnr()) && b:gitgutter.path != -2
		" Use GitGutter
		let l:cmd = "\<Plug>".'(GitGutter'. (a:next ? 'Next' : 'Prev') .'Hunk)'
	endif
	" Center and open folds
	let l:cmd .= 'zzzv'
	execute 'normal ' . l:cmd
endfunction "}}}
nnoremap <silent> du :<C-u>diffupdate<CR>
" After changing something in a diff, move to the next diff
nmap do do]c
nmap dp dp]c
" Diff the current file - VCS {{{
" These diffs are on the opposite side from the usual
nnoremap <silent> <Leader>d :topleft Gdiffsplit<CR>
nnoremap <silent> <Leader>s :topleft Gdiffsplit HEAD<CR>
nnoremap <Leader>S :topleft Gdiffsplit<Space>
" TODO: Do something similar for signify, if possible
" One-shot diff of the current file
" This is plugin-specific
nnoremap <silent> <Leader>D :call DiffOneShot()<CR>
function! DiffOneShot()
	" Git Gutter
	if get(b:gitgutter, 'enabled', 1) == 0
		call s:DiffOneShot_GitGutter()
 	elseif !sy#buffer_is_active()
 		call s:DiffOneShot_Signify()
	endif
endfunction "}}}

" Splits
set splitright " New splits to the right
set splitbelow " New splits below
" When returning to a buffer, show the current line always
augroup vimrc_split | autocmd!
	autocmd BufEnter * normal! zv
augroup END

" Tabs
nnoremap <silent> gt :call <SID>vimrc_tabs_rename()<CR>
function! s:vimrc_tabs_rename()
	call inputsave()
	let l:tab_name = input('Tab Name: ', get(t:, 'tab_name', ''))
	call inputrestore()
	if empty(l:tab_name)
		if exists('t:tab_name')
			unlet t:tab_name
		endif
	else
		let t:tab_name = l:tab_name
	endif
	LightlineReload
endfunction
"}}}

" Whitespace Management {{{
set autoindent smartindent "Indent in smart, language-specific ways
set shiftround "Align to the nearest "shiftwidth"
" Enter {{{
" On normal mode, Enter puts a new line after the current one
nnoremap <silent> <CR> :<C-u>put =repeat(nr2char(10), v:count1)<CR>
" And S-Enter puts it before, but keep the cursor in the same place
nnoremap <silent> <S-CR> :<C-u>call <SID>enter_lines_up(v:count1)<CR>
" Synonym for terminals
nmap g<CR> <S-CR>
" Helper Functions {{{
function! s:enter_lines_up(count)
	let [l:bufnum, l:lnum, l:cnum, l:offset] = getpos('.')
	put! =repeat(nr2char(10), a:count)
	call setpos('.', [l:bufnum, l:lnum + a:count, l:cnum, l:offset])
endfunction
"}}}
"}}}
" Tab {{{
set smarttab "Use 'shiftwidth' with tabs
" Tab to indent text by shiftwidth columns
" C-Tab to indent text by 1 column, using spaces
function! IndentLine(cols)
	if a:cols > 0
		let l:pat = '^'
		let l:subs = repeat(' ',a:cols)
	elseif a:cols < 0
		" The last x spaces, where x < -a:cols
		" If that doesn't match, just ^
		" TODO: Remove tabs?
		let l:pat = '\v^\s{-}\zs {1,'.-a:cols.'}\ze\S|^'
		let l:subs = ''
	else
		return 1 "Do nothing
	endif
	execute 'keeppatterns substitute' '/'.l:pat.'/'.l:subs.'/'
	return 0
endfunction
command! -nargs=? -range IndentLine <line1>,<line2>call IndentLine(<f-args>)
nnoremap <Tab> >>
nnoremap <S-Tab>  <<
nnoremap <silent> <C-Tab> :IndentLine 1<CR>
nnoremap <silent> <C-S-Tab>  :IndentLine -1<CR>
nmap g<Tab> <C-Tab>
nmap g<S-Tab> <C-S-Tab>
" On visual mode, keep the indented text select
xnoremap <Tab> >gv
xnoremap <S-Tab>  <gv
xnoremap <silent> <C-Tab> :IndentLine 1<CR>gv
xnoremap <silent> <C-S-Tab> :IndentLine -1<CR>gv
xmap g<Tab> <C-Tab>
xmap g<S-Tab> <C-S-Tab>
"
"}}}
" Split the line at this point (replaces current char)
" gS reshuffles the resulting lines
nnoremap <silent> gs r<CR>
nmap     <silent> gS gs-j
"}}}

" Search and Replace {{{
set ignorecase smartcase "Sane defaults
set incsearch "Start searching right away
set inccommand=nosplit "Show the result of the substitute in the buffer right away
nmap <silent> <C-l> <Plug>(vimrc-clear-hls)
" Center on search
" Make sure it works on folds
nnoremap * *zvzz
nnoremap n nzvzz
nnoremap N Nzvzz
" Don't mess with search directions, n is ALWAYS forward
nmap # *NN
nmap g# g*NN
xmap # *NN
xmap g# g*NN
" 'gr' to start a full-file search and replace for the current pattern
" Also works on visual mode
nnoremap gr :<C-u>%s@<C-r>/@@<Left>
xnoremap gr :s@<C-r>/@@<Left>
" '&' to repeat last ':s', use flags too
nnoremap & :&&<CR>
xnoremap & :&&<CR>
"}}}

" Move lines {{{
" Accept a count
" Normal Mode: Move the current line
nnoremap <silent> + :<C-u>move +<C-r>=v:count1<CR><CR>
nnoremap <silent> - :<C-u>move -<C-r>=v:count1 + 1<CR><CR>
" Visual Mode: Move the current selection
xnoremap <silent> + :call <SID>MoveCurrentBlock(<C-r>=v:count1<CR>)<CR>gv
xnoremap <silent> - :call <SID>MoveCurrentBlock(-<C-r>=v:count1<CR>)<CR>gv
" Helper Functions {{{
function! s:MoveCurrentBlock(offset) range
	" Only on V-Line visual mode
	if !a:offset || a:offset == 0 || visualmode() !=# 'V'
		return
	endif
	let l:range = a:lastline - a:firstline + 1
	if a:offset > 0
		let l:move = a:lastline + a:offset
	else
		let l:move = a:firstline + a:offset - 1
	endif
	if l:move > line('$')
		return
	endif
	execute a:firstline.','.a:lastline.'move '.l:move
endfunction
"}}}
"}}}

" Help {{{
nnoremap <Plug>(vimrc-help) :<C-u>help<Space>
" <F1> for choosing help
" <Leader><F1> for access the vim help using Denite
nmap <F1> <Plug>(vimrc-help)
nnoremap <silent> <Leader><F1> :MyDenite -buffer-name=Help help<CR>
" g<F1> to close the help, regardless of current buffer
nnoremap <silent> <C-F1> :helpclose<CR>
nmap g<F1> <C-F1>
"}}}

" Spell {{{
let g:spelling_filetypes = [
			\   'text',
			\   'mkd', 'markdown',
			\   'liquid',
			\   'rst',
			\   'asciidoc',
			\   'mail',
			\   'gitcommit',
			\   'help',
			\ ]
set nospell "Disable it by default
set spelllang=en "Just a sensible default
set spellfile= "Auto-discover
set spellsuggest=fast "Works reasonably enough, but it's really fast, especially for English
set spellsuggest+=25 "Show at most this suggestions
let g:loaded_spellfile_plugin = 1 "Don't ask for downloading spellfiles
function! s:vimrc_text()
	" Initialize the spelling plugins
	call lexical#init({'spell': 1})
	call SpellLoop_Init()
	" `=` reflows the text, instead of formatting
	noremap <silent> <buffer> = gq
	" `==` formats the entire buffer properly, instead of indenting
	" Keep the current location and leverage the "textobj-entire" plugin
	nmap <silent> <buffer> == m`gqa<CR>'`
	runtime scripts/word_wrapping.vim
	runtime scripts/movement_display.vim
endfunction
augroup vimrc_spelling | autocmd!
	" Mark these files as "text"
	execute 'autocmd FileType' join(g:spelling_filetypes,',') 'call s:vimrc_text()'
augroup END
"}}}

" Navigation {{{
nnoremap <Space> <C-d>
nnoremap <S-Space> <C-u>
nmap g<Space> <S-Space>
" Tabs {{{
nnoremap <silent> <M-h> :tabprevious<CR>
nnoremap <silent> <M-l> :tabnext<CR>
nnoremap <silent> <M-q> :tabclose<CR>
nnoremap <silent> <M-H> :tabmove -1<CR>
nnoremap <silent> <M-L> :tabmove +1<CR>
"}}}
set hidden "Don't prompt when changing buffers
" Alt-jk to move around {{{
nnoremap <M-j> <C-e>
nnoremap <M-k> <C-y>
"}}}
"On visual-block let the cursor go to "illegal" places (Half-tabs, past the end, etc)
set virtualedit=block
" Jumps {{{
nnoremap <silent> g: :MyDenite jump<CR>
" Jump back
nnoremap <silent> <C-o> <C-o>zzzv
" Jump forward
nnoremap g. <C-i>zzzv
"}}}

" "Smart" Apostrophe {{{
nnoremap ' `
nnoremap ` '
"}}}
" "Smart" Zero {{{
nnoremap 0 ^
nnoremap ^ 0
"}}}
" Smart Underscore {{{
function! SmartUnderscore()
	let l:line = line('.')
	let l:col  = col('.')

	execute 'normal! ^'
	let l:soft = col('.')

	execute 'normal! 0'
	let l:hard = col('.')

	if l:col != l:soft
		call cursor(l:line, l:soft)
	else
		call cursor(l:line, l:hard)
	endif
endfunction
nnoremap <silent> _ :<C-u>call SmartUnderscore()<CR>
"}}}

" Quickfix and Location List {{{
" See: https://github.com/romainl/vim-qf
" See: https://github.com/blueyed/vim-qf_resize
" Override: let g:vimrc_listings_maxsize = &previewheight
command! -nargs=? LoclistOpen  if len(getloclist(0)) > 0 | lopen <args> | endif
command! -nargs=? QuickfixOpen if len(getqflist()) > 0   | copen <args> | endif
" Quickfix Quick Navigation
nnoremap <silent> <Leader>q :QuickfixOpen<CR>
nnoremap <silent> [q :cprevious<CR>zzzv
nnoremap <silent> ]q :cnext<CR>zzzv
" Location List Quick Navigation
nnoremap <silent> <Leader>l :LoclistOpen<CR>
nnoremap <silent> [l :lprevious<CR>zzzv
nnoremap <silent> ]l :lnext<CR>zzzv
"}}}
" Arguments List {{{
function! s:vimrc_args_quickfix()
	" Move all arguments to a new quickfix list
	call setqflist(map(argv(), {idx, f ->
				\ 	{
				\ 		'filename': f,
				\ 		'text': printf('Argument %d: %s', idx, f)
				\ 	}
				\ }))
	" Setup that quickfix title
	call setqflist([], 'r', {'title': 'Arguments'})
	" TODO: Migrate to a single `setqflist` call, using 'efm'
	ArgsCleanup
endfunction
command! ArgsCleanup %argdelete
command! ArgsQuickfix call s:vimrc_args_quickfix()
nnoremap <silent> g<M-h> :previous<CR>
nnoremap <silent> g<M-l> :next<CR>
"}}}
"}}}

" Plugin Configuration
" Fugitive {{{
let g:fugitive_gitlab_domains = []
command! GstatusClose call s:vimrc_fugitive_gstatusclose()
augroup vimrc_fugitive | autocmd!
	autocmd User Fugitive call s:vimrc_setup_fugitive()
augroup END
" General-purpose fugitive mapping
nnoremap          <Leader>g<Space> :Git<Space>
nnoremap <silent> <Leader>gs :Git<CR>
nnoremap <silent> <Leader>gc :Git commit<CR>
nnoremap <silent> <Leader>gC :Git commit --all<CR>
nnoremap <silent> <Leader>ga :Git commit --amend --reset-author --no-edit<CR>
nnoremap <silent> <Leader>gA :Git commit --amend --reset-author<CR>
nnoremap <silent> <Leader>gj :Dispatch! git fetch --all<CR>
nnoremap <silent> <Leader>gJ :Dispatch! git pull<CR>
nnoremap <silent> <Leader>gk :Dispatch! git push<CR>
function! s:vimrc_setup_fugitive()
	command! -buffer -nargs=? -complete=custom,FugitiveCompleteRemoteBranches GitUpstream call s:vimrc_git_upstream(<f-args>)
	" Stage the current file
	nnoremap <buffer> <silent> <Leader>gw :call <SID>vimrc_git_stage()<CR>
	" Unstage the current file
	nnoremap <buffer> <silent> <Leader>gr :call <SID>vimrc_git_unstage()<CR>
	" Change to an existing branch
	nnoremap <buffer> <silent> <Leader>gb :call <SID>vimrc_git_changebranch()<CR>
	" Create a new branch
	nnoremap <buffer> <silent> <Leader>gB :call <SID>vimrc_git_newbranch()<CR>
	" Change upstream branch
	nnoremap <buffer> <silent> <Leader>gu :GitUpstream<CR>
endfunction
" Helper functions {{{
function! FugitiveCompleteRemoteBranches(A, L, P) abort
	return system(FugitiveShellCommand('rev-parse', '--symbolic', '--remotes'))
endfunction
function! FugitiveCompleteBranches(A, L, P) abort
	return system(FugitiveShellCommand('rev-parse', '--symbolic', '--branches'))
endfunction
function! s:vimrc_git_stage()
	Gwrite
endfunction
function! s:vimrc_git_unstage()
	echon system(FugitiveShellCommand('reset', '--quiet', '--', expand('%:p')))
	GitGutter "Update the gutter
endfunction
function! s:vimrc_git_changebranch()
	call inputsave()
	let l:branch = input({
				\ 'prompt': 'Change Branch: ',
				\ 'completion': 'custom,FugitiveCompleteBranches',
				\ })
	call inputrestore()
	echo ' '
	if !empty(l:branch)
		execute 'Git' 'checkout' l:branch
	endif
endfunction
function! s:vimrc_git_newbranch()
	call inputsave()
	let l:newbranch = input('New Branch: ')
	call inputrestore()
	echo ' '
	if !empty(l:newbranch)
		execute 'Git' 'checkout' '-b' l:newbranch
	endif
endfunction
function! s:vimrc_git_upstream(...)
	if a:0
		let l:upstream = a:1
	else
		call inputsave()
		let l:upstream = input({
					\ 'prompt': 'Set Upstream To: ',
					\ 'completion': 'custom,FugitiveCompleteRemoteBranches',
					\ })
		call inputrestore()
		echo ' '
	endif
	if !empty(l:upstream)
		execute 'Git' 'branch' '-u' l:upstream
	endif
endfunction
function! s:vimrc_fugitive_gstatusclose()
	for l:winnr in range(1, winnr('$'))
		if !empty(getwinvar(l:winnr, 'fugitive_status'))
			execute l:winnr.'close'
		endif
	endfor
endfunction
"}}}
"}}}
" Denite {{{
let s:denite_date_format = '%Y-%m-%d %H:%M'
let s:ignore_globs = g:root_markers_vcs + [g:root_marker_manual]
" Globs {{{
let s:ignore_globs__ripgrep = []
let s:ignore_globs__ag = []
for s:g in s:ignore_globs
	let s:ignore_globs__ripgrep += ['-g','!'.s:g]
	let s:ignore_globs__ag += ['--ignore',s:g]
endfor
unlet s:g
"}}}
" Configuration
"" Buffer:
call denite#custom#alias('source', 'project/buffer', 'buffer')
call denite#custom#source('project/buffer', 'matchers',
			\ ['matcher_project_files', 'matcher_fuzzy'] )
call denite#custom#var('buffer,project/buffer', 'date_format', s:denite_date_format)

"" File: git-changes: Modified + Untracked - Ignored
call denite#custom#alias('source', 'file/git_changes', 'file/rec')
call denite#custom#var('file/rec/git-changes', 'command',
			\ ['git','ls-files','--modified','--others','--exclude-standard'] )

"" Recursive File: git
call denite#custom#alias('source', 'file/rec/git', 'file/rec')
call denite#custom#var('file/rec/git', 'command',
			\ ['git','ls-files','--cached','--exclude-standard'] )
"" Recursive File: ag
call denite#custom#alias('source', 'file/rec/ag', 'file/rec')
call denite#custom#var('file/rec/ag', 'command',
			\ ['ag', '--follow', '--nocolor', '--nogroup', '--hidden', '-g', ''] + s:ignore_globs__ag )
"" Recursive File: ripgrep
call denite#custom#alias('source', 'file/rec/ripgrep', 'file/rec')
call denite#custom#var('file/rec/ripgrep', 'command',
			\ ['rg', '--files', '--hidden'] + s:ignore_globs__ripgrep )

"" Grep:
call denite#custom#var('grep', 'default_opts',
			\ ['--ignore-case','-nH'] )
call denite#custom#source('grep', 'args', ['','','!'])
"" Grep: git grep
call denite#custom#alias('source', 'grep/git', 'grep')
call denite#custom#var('grep/git', 'default_opts',
			\ ['grep', '--ignore-case','-nH'] )
"" Grep: ag
call denite#custom#alias('source', 'grep/ag', 'grep')
call denite#custom#var('grep/ag', 'command', ['ag'])
call denite#custom#var('grep/ag', 'default_opts',
			\ ['--smart-case', '--vimgrep', '--hidden'] + s:ignore_globs__ag )
call denite#custom#var('grep/ag', 'recursive_opts', [])
call denite#custom#var('grep/ag', 'pattern_opt', [])
call denite#custom#var('grep/ag', 'separator', ['--'])
call denite#custom#var('grep/ag', 'final_opts', [])
"" Grep: ripgrep
call denite#custom#alias('source', 'grep/ripgrep', 'grep')
call denite#custom#var('grep/ripgrep', 'command', ['rg'])
call denite#custom#var('grep/ripgrep', 'default_opts',
			\ ['--vimgrep', '--no-heading', '--hidden'] + s:ignore_globs__ripgrep )
call denite#custom#var('grep/ripgrep', 'recursive_opts', [])
call denite#custom#var('grep/ripgrep', 'pattern_opt', ['--regexp'])
call denite#custom#var('grep/ripgrep', 'separator', ['--'])
call denite#custom#var('grep/ripgrep', 'final_opts', [])

" MRU:
call denite#custom#var('file_mru', 'fnamemodify', ':~')
call denite#custom#alias('source', 'project/file_mru', 'file_mru')
call denite#custom#var('project/file_mru', 'fnamemodify', ':.')
call denite#custom#source('project/file_mru', 'matchers',
			\ ['matcher_project_files', 'matcher_fuzzy'] )
call denite#custom#source('project/file_mru', 'converters',
			\ ['converter_relative_word'] )

" KeyMaps
call denite#custom#map('insert','<C-j>', '<denite:move_to_next_line>','noremap')
call denite#custom#map('insert','<C-k>', '<denite:move_to_previous_line>','noremap')
call denite#custom#map('insert','<C-s>', '<denite:do_action:split>','noremap')
call denite#custom#map('insert','<C-v>', '<denite:do_action:vsplit>','noremap')
call denite#custom#map('insert','<C-t>', '<denite:do_action:tabopen>','noremap')
call denite#custom#map('insert','<C-Space>', '<denite:do_action:preview>','noremap')
call denite#custom#map('insert','<C-p>', '<denite:assign_previous_text>','noremap')
call denite#custom#map('insert','<C-n>', '<denite:assign_next_text>','noremap')
"" EMACS/Readline compatible (mostly)
call denite#custom#map('insert','<C-b>', '<denite:move_caret_to_left>', 'noremap')
call denite#custom#map('insert','<C-f>', '<denite:move_caret_to_right>', 'noremap')
call denite#custom#map('insert','<A-b>', '<denite:move_caret_to_one_word_left>', 'noremap')
call denite#custom#map('insert','<A-f>', '<denite:move_caret_to_one_word_right>', 'noremap')
call denite#custom#map('insert','<C-a>', '<denite:move_caret_to_head>', 'noremap')
call denite#custom#map('insert','<C-e>', '<denite:move_caret_to_tail>', 'noremap')
call denite#custom#map('insert','<C-d>', '<denite:delete_char_under_caret>', 'noremap')
call denite#custom#map('insert','<A-C-v>', '<denite:paste_from_default_register>','noremap')
"" Extra bindings
""" Move all results to the quickfix
call denite#custom#map('insert', '<C-g>q', '<denite:multiple_mappings:denite:toggle_select_all,denite:do_action:quickfix>', 'noremap')

" Helper Suffix {{{
let g:denite_helper_suffix = s:helper_program
function! s:denite_helper_suffix()
	let l:suffix = get(b:, 'denite_helper_suffix', g:denite_helper_suffix)
	if l:suffix !=# '' " Prepend '/' if found
		let l:suffix = '/' . l:suffix
	endif
	return l:suffix
endfunction
"}}}
augroup vimrc_denite | autocmd!
	" Don't set the buffer variable if the global is defined
	" Fugitive: On git buffers, use git
	autocmd User FugitiveBoot if g:denite_helper_suffix == '' && b:git_dir != '' | let b:denite_helper_suffix = 'git' | endif
	autocmd User FugitiveBoot nnoremap <silent> <buffer> <Leader>gl :MyDenite -buffer-name=GitFOI file/git_changes<CR>
augroup END
command! -nargs=+ -range -complete=customlist,denite#helper#complete
      \ MyDenite
      \ call denite#helper#call_denite('Denite',
      \                                  '-no-statusline -auto-resize -cursor-wrap -reversed '.<q-args>,
			\                                <line1>, <line2>)
"" CtrlP replacement
nnoremap <silent> <C-p> :MyDenite -buffer-name=Files file/rec`<SID>denite_helper_suffix()`<CR>
"" Grep in the current project interactively
""" Start search right away
nnoremap <silent> g/ :MyDenite -buffer-name=Search -no-auto-resize grep`<SID>denite_helper_suffix()`:::!<CR>
""" Use the current word as initial input
nnoremap <silent> <Leader>* :MyDenite -buffer-name=Search -input="`expand('<cword>')`" grep`<SID>denite_helper_suffix()`:::!<CR>
"" Grep in the current project non-interactively
""" That is, input a pattern and then filter it in an regular denite buffer
nnoremap <silent> <Leader>/ :MyDenite -buffer-name=Search grep`<SID>denite_helper_suffix()`<CR>
"" MRU files
nnoremap <silent> <C-b> :MyDenite -buffer-name=FileMRU file_mru<CR>
"" MRU directories. Poor man's MRU projects
nnoremap <silent> g<C-b> :MyDenite -buffer-name=DirMRU directory_mru<CR>
"" Project file chooser. Combines project buffers, project MRU and project files, by this order.
"" This is my default mapping to move between files intra-project
nnoremap <silent> <C-Space> :MyDenite -buffer-name=Everything -unique project/buffer project/file_mru file/rec`<SID>denite_helper_suffix()`<CR>
"" Outline the current file, using ctags
nnoremap <silent> gO :MyDenite -buffer-name=Outline outline<CR>
"" Search the current word in the outline. If there is a single result, jump to it
nnoremap <silent> <Leader>f :MyDenite -buffer-name=Outline -immediately-1 -input="`expand('<cword>')`" outline<CR>
"" Auto-completable Denite prompt
nnoremap <Leader><Space> :MyDenite<Space>
"}}}
" Git Gutter {{{
let g:gitgutter_map_keys = 0 "Map keys manually
nmap <Leader>hs <Plug>(GitGutterStageHunk)
nmap <Leader>hr <Plug>(GitGutterUndoHunk)
nmap <Leader>hp <Plug>(GitGutterPreviewHunk)

let g:gitgutter_preview_win_floating = 0  " Regular preview windows
let g:gitgutter_show_msg_on_hunk_jumping = 0
highlight link GitGutterAdd    GutterSignAdd
highlight link GitGutterChange GutterSignChange
highlight link GitGutterDelete GutterSignDelete
let g:gitgutter_sign_added = s:glyph_sign_added
let g:gitgutter_sign_modified = s:glyph_sign_modified
let g:gitgutter_sign_removed = s:glyph_sign_removed
let g:gitgutter_sign_modified_removed = s:glyph_sign_modelete
let g:gitgutter_sign_removed_above_and_below = s:glyph_sign_removedfirst
" One-shot "diff"
function! s:DiffOneShot_GitGutter()
	call gitgutter#buffer_enable()
	GitGutter
	sleep 500 m
	call gitgutter#utility#setbufvar(bufnr(), 'enabled', 0)
endfunction
"}}}
" Signify {{{
" GitGutter takes care of the git elephant in the room
" This picks up the stragglers
" This is a whitelist of VCS to consider
let g:signify_skip = {'vcs': { 'allow': ['svn'] }}

let g:signify_sign_show_count = 0 "Simple signs, gitgutter-style
" Can't believe it's not GitGutter
highlight link SignifySignAdd    GitGutterAdd
highlight link SignifySignChange GitGutterChange
highlight link SignifySignDelete GitGutterDelete
let g:signify_sign_add = s:glyph_sign_added
let g:signify_sign_delete = s:glyph_sign_removed
let g:signify_sign_delete_first_line = s:glyph_sign_removedfirst
let g:signify_sign_change = s:glyph_sign_modified
let g:signify_sign_changedelete = s:glyph_sign_modelete
" One-shot "diff"
function! s:DiffOneShot_Signify()
	SignifyEnable
	SignifyDisable
endfunction
"}}}
" NeoMRU {{{
let g:neomru#time_format = s:denite_date_format
"}}}
" Rooter {{{
let g:rooter_patterns = g:root_markers
let g:rooter_disable_map = 1
let g:rooter_cd_cmd = 'lcd'
let g:rooter_silent_chdir = 1
nnoremap <silent> <Leader>cd :Rooter<CR>
"}}}
" Text Objects {{{
let g:textobj_entire_no_default_key_mappings = 1
call textobj#user#map('entire', {
			\ 	'-': {
			\ 		'select-a': 'a<CR>',
			\ 		'select-i': 'i<CR>',
			\ 	}
			\ })
let g:textobj_function_no_default_key_mappings = 1
call textobj#user#map('function', {
			\ 	'a': {
			\ 		'select': 'a<LocalLeader>f',
			\ 	},
			\ 	'i' : {
			\ 		'select': 'i<LocalLeader>f',
			\ 	},
			\ })
let g:textobj_comment_no_default_key_mappings = 1
call textobj#user#map('comment', {
			\ 	'-': {
			\ 		'select-a': 'a#',
			\ 		'select-i': 'i#',
			\ 	},
			\ 	'big': {
			\ 		'select-a': 'a<LocalLeader>#',
			\ 	},
			\ })
omap ic <Plug>(GitGutterTextObjectInnerPending)
omap ac <Plug>(GitGutterTextObjectOuterPending)
xmap ic <Plug>(GitGutterTextObjectInnerVisual)
xmap ac <Plug>(GitGutterTextObjectOuterVisual)
"}}}
" Surround {{{
" TODO: Investigate https://github.com/rhysd/vim-operator-surround
" After surrounding with multilines `yS`, indent with `=`
let g:surround_indent = 1
" <CR>: Newlines above and below
let g:surround_13 = "\n\r\n\n"
"}}}
" Neomake |> Syntastic {{{
" Configuration
let g:neomake = {
			\ 'error_sign': {
			\   'text': s:type_glyph['E'],
			\ },
			\ 'warning_sign': {
			\   'text': s:type_glyph['W'],
			\ },
			\ 'info_sign': {
			\   'text': s:type_glyph['I'],
			\ },
			\ 'message_sign': {
			\   'text': s:type_glyph['M'],
			\ },
			\ }
"Immediately after reading/writing a buffer
call neomake#configure#automake('rw')
"After a small delay in normal mode
call neomake#configure#automake('n', 2500)
" Mappings
"  Run Neomake manually
nnoremap <silent> <Leader>tm :NeomakeToggleBuffer<CR>
nnoremap <silent> <Leader>tM :NeomakeToggle<CR>
nnoremap <silent> Q :Neomake<CR>
"}}}
" SuperTab {{{
" Smarter completion
" Real fallback is the old regular keyword completion
let g:SuperTabDefaultCompletionType = 'context'
let g:SuperTabContextDefaultCompletionType = '<C-n>'
let g:SuperTabClosePreviewOnPopupClose = 0
function! s:vimrc_supertab_configure()
	if &omnifunc !=# ''
		call SuperTabChain(&omnifunc, g:SuperTabContextDefaultCompletionType, 0)
		if exists('b:supertab_chain_default') && b:supertab_chain_default == 1
			call SuperTabSetDefaultCompletionType('<C-x><C-u>')
		endif
	endif
endfunction
augroup vimrc_supertab_chain | autocmd!
	" See supertab-completionchaining
	" This filetypes will have omnifunc override the context completion
	autocmd FileType css,sql,php,python,cs let b:supertab_chain_default = 1
	" By default, don't override
	autocmd FileType * call s:vimrc_supertab_configure()
augroup END
" Each completion is a different beast
let g:SuperTabRetainCompletionDuration = 'completion'
" Insert completion, write more, and Tab again to re-complete
" But don't pre-select the first value. This works like wildmode, mostly
let g:SuperTabLongestEnhanced = 1
let g:SuperTabLongestHighlight = 0
" C-Tab to do full line completion
let g:SuperTabSkipMappingTabLiteral = 1
imap <C-Tab> <C-x><C-l>
"}}}
" Markdown @ tpope {{{
let g:markdown_fenced_languages = s:fenced_languages
let g:markdown_minlines = 250
"}}}
" Liquid @ tpope {{{
let g:liquid_highlight_types = s:fenced_languages
"}}}
" Undotree {{{
nnoremap <silent> <Leader>u :UndotreeToggle<CR>
nnoremap <silent> <Plug>(vimrc-close-plugin-undotree) :UndotreeClose<CR>
let g:undotree_WindowLayout = 4 " Right-side
let g:undotree_ShortIndicators = 1
" Diff is a bit broken, see Extradite
let g:undotree_DiffAutoOpen = 0
let g:undotree_DiffpanelHeight = 15
let g:undotree_SetFocusWhenToggle = 1
let g:undotree_TreeNodeShape = '•'
function! g:Undotree_CustomMap()
	nmap <buffer> l <Plug>UndotreeEnter
endfunction
augroup vimrc_undotree | autocmd!
	" General Cleanup: Hide undotree
	autocmd User VimRC_CleanWindows UndotreeHide
augroup END
"}}}
" Lightline {{{
set noshowmode
let g:lightline = {}
" Commands {{{
command! -nargs=0 LightlineToggle call lightline#toggle()
command! -nargs=0 LightlineReload call s:vimrc_lightline_reload()
command! -nargs=1 -complete=custom,s:vimrc_lightline_colorscheme_completions LightlineColorscheme
			\ call s:vimrc_lightline_colorscheme(<q-args>)

" Helper Functions {{{
" Change Colorscheme
" https://github.com/itchyny/lightline.vim/issues/258
function! s:vimrc_lightline_colorscheme(name)
	let g:lightline.colorscheme = a:name
	call s:vimrc_lightline_reload()
endfun
function! s:vimrc_lightline_colorscheme_completions(...)
	return join(map(
				\ globpath(&runtimepath, 'autoload/lightline/colorscheme/*.vim', v:true, v:true),
				\ "fnamemodify(v:val,':t:r')"),
				\ "\n")
endfun

" Reload internal state
"" https://github.com/itchyny/lightline.vim/issues/241
function! s:vimrc_lightline_reload()
	call lightline#init()
	call lightline#colorscheme()
	call lightline#update()
endfunction

" Fugitive Commit beautifier
function! s:vimrc_lightline_fugitive_beautify(commit)
	" Show nice name for branches and tags only
	let l:string = systemlist(FugitiveShellCommand('name-rev', '--name-only', '--always',
				\ a:commit))[0]
	" Show remote branches differently
	return substitute(l:string, '\v^remotes/([^/]+)/', '\1:', '')
endfunction
"}}}
"}}}
" Active windows
let g:lightline.active = {
			\ 'left': [
			\   [ 'my_mode' ],
			\   [ 'my_buffername', 'my_modified', 'truncate_here' ],
			\   [ 'my_vcs', 'my_plugininfo_extra', 'my_flags' ]
			\ ],
			\ 'right': [
			\   [ 'my_wordcount', 'my_arglist', 'my_percent', 'my_lineinfo' ],
			\   [ 'my_fileformat', 'my_fileencoding', 'my_filetype' ],
			\   [ 'my_spell_extra', 'my_maker', 'my_plugininfo' ],
			\ ]
			\ }
" Inactive windows
let g:lightline.inactive = {
			\ 'left': [
			\ 	[ 'my_header' ],
			\   [ 'my_buffername', 'my_modified', 'truncate_here' ]
			\ ],
			\ 'right': [
			\   [ 'my_percent', 'my_lineinfo_extra' ],
			\   [ 'my_spell', 'my_maker', 'my_plugininfo' ],
			\ ],
			\ }
" Tabs
let g:lightline.tabline = {
			\ 	'left': [ [ 'tabs' ] ],
			\ 	'right': []
			\ }
let g:lightline.tab = {
			\ 'active': [ 'my_tabname', 'my_modified', 'my_buffernum' ],
			\ 'inactive': [ 'my_tabname', 'my_modified', 'my_buffernum' ]
			\ }
" Internal Configuration
let s:lightline_small_screen = 80
let s:lightline_default_fileformat = split(&fileformats, ',')[0]
let s:lightline_default_fileencoding = 'utf-8' "Not the one with BOM
" Components {{{
let g:lightline.component = {
			\ 'my_percent': '%P:%L',
			\ 'truncate_here': '%<',
			\ }
let g:lightline.component_function = {
			\ 'my_header': 'LightlineHeader',
			\ 'my_buffername': 'LightlineBufferName',
			\ 'my_filetype': 'LightlineFiletype',
			\ 'my_lineinfo': 'LightlineLineInfo',
			\ 'my_arglist': 'LightlineArgList',
			\ 'my_lineinfo_extra': 'LightlineLineInfoExtra',
			\ 'my_mode': 'LightlineMode',
			\ 'my_modified': 'LightlineModified',
			\ 'my_plugininfo': 'LightlinePluginInfo',
			\ 'my_plugininfo_extra': 'LightlinePluginInfoExtra',
			\ 'my_vcs': 'LightlineVCS',
			\ 'my_wordcount': 'LightlineWordcount',
			\ 'my_maker': 'LightlineNeomake',
			\ 'my_spell': 'LightlineSpell',
			\ 'my_spell_extra': 'LightlineSpellExtra',
			\ }
let g:lightline.component_expand = {
			\ 'my_fileencoding': 'LightlineFileEncoding',
			\ 'my_fileformat': 'LightlineFileFormat',
			\ 'my_flags': 'LightlineFlags',
			\ }
let g:lightline.tab_component_function = {
			\ 'my_tabname': 'LightlineTablineName',
			\ 'my_buffernum': 'LightlineTablineBuffers',
			\ 'my_modified': 'LightlineTablineModified',
			\ }
let g:lightline.component_visible_condition = {
			\ 'truncate_here': 0,
			\ }
let g:lightline.component_type = {
			\ 'truncate_here': 'raw',
			\ 'my_fileencoding': 'warning',
			\ 'my_fileformat': 'warning',
			\ 'my_flags': 'warning',
			\ }

function! LightlineHeader()
	" Main "special buffer" detection function
	" Keep this lean, it's invoked many times
	if &diff
		" Special-case diff buffers
		if exists('b:differ')
			return 'LINEDIFF'
		else
			return 'DIFF'
		endif
	elseif &filetype ==# 'undotree' "Plugin: Undotree
		return 'UNDOTREE'
	elseif &filetype ==# 'gitcommit' "Plugin: Fugitive
		" These windows are previews too
		" Keep it above &previewwindow
		return 'GIT-COMMIT'
	elseif &filetype ==# 'git'
		return 'GIT'
	elseif &previewwindow && exists('b:db')
		" These windows are previews too
		" Keep it above &previewwindow
		return 'DADBOD'
	elseif &previewwindow "Preview Window
		return 'PREVIEW'
	elseif &filetype ==# 'help' "Help
		if b:subtype ==# 'buffer'
			return 'HELP'
		endif
	elseif &filetype ==# 'qf'
		if b:subtype ==# 'quickfix'
			return 'QUICKFIX'
		elseif b:subtype ==# 'loclist'
			return 'LOCATION LIST'
		endif
	elseif &filetype ==# 'man' "Plugin: Man
		return 'MAN'
	elseif &filetype ==# 'denite' "Plugin: Denite
		return 'DENITE'
	elseif &filetype ==# 'pydoc'
		return 'PYDOC'
	elseif &filetype ==# 'diff'
		if exists('t:diffpanel') &&
					\ expand('%') ==# t:diffpanel.bufname "Plugin: Undotree
			return 'UNDOTREE DIFF'
		endif
	elseif &filetype ==# 'GV'
		return 'GV'
	elseif &filetype ==# 'dirvish'
		return 'DIRVISH'
	elseif has_key(b:, 'terminal_job_id')  " This seems ugly
		return 'TERMINAL'
	elseif has_key(b:, 'fugitive_type')  " This is also a Fugitive buffer
		return 'GIT'
	endif
	return ''
endfunction

function! LightlineBufferName()
	let l:header = LightlineHeader()
	let l:bufname = expand('%:.') "Equivalent to 'relativepath'
	if !empty(l:header) " Special Buffers
		if index(['HELP', 'MAN'], l:header) != -1
			return expand('%:t:r') "Filename without extension
		elseif l:header ==# 'DENITE'
			return denite#get_status('buffer_name')
		elseif l:header ==# 'PYDOC'
			return b:pydoc_name
		elseif l:header ==# 'DIFF'
			if exists('b:git_dir') && get(b:, 'fugitive_type', '') ==# 'blob'
				return FugitivePath(@%, '')
			else
				return l:bufname
			endif
		elseif l:header ==# 'TERMINAL'
			let l:term_regex = '\v^term://.+//\d+:'
			return substitute(b:term_title, l:term_regex, '', '')
		elseif l:header ==# 'DADBOD'
			return b:db
		elseif l:header ==# 'GV'
			if exists('b:gv')
				return b:gv['opts']['args']
			endif
			return substitute(bufname('%'), '^.\{-\} ', '', '')
		elseif l:header ==# 'GIT'
			let l:commit = matchstr(FugitiveParse()[0], '^\x\+')
			if index(['tree', 'blob'], b:fugitive_type) != -1
				return printf('%s @ %s',
							\ {'tree':'Tree', 'blob':'File'}[b:fugitive_type],
							\ s:vimrc_lightline_fugitive_beautify(l:commit))
			elseif b:fugitive_type ==# 'commit'
				return s:vimrc_lightline_fugitive_beautify(l:commit)
			elseif b:fugitive_type ==# 'index'
				return 'Status'
			else
				return printf('Type: %s', b:fugitive_type)
			endif
		elseif l:header ==# 'DIRVISH'
			return expand('%:~:h')
		elseif l:header ==# 'LINEDIFF'
			return bufname(b:differ.original_buffer)
		endif
		return ''
	else
		return l:bufname
	endif
endfunction
function! LightlineFileEncoding()
	" Don't show the default file encoding
	if &fileencoding ==# s:lightline_default_fileencoding
		return ''
	else
		return &fileencoding
	endif
endfunction
function! LightlineFileFormat()
	" Don't show the default fileformat
	if &fileformat ==# s:lightline_default_fileformat
		return ''
	else
		return &fileformat
	endif
endfunction
function! LightlineFiletype()
	let l:header = LightlineHeader()
	" Hide on Special Buffers, except...
	if !empty(l:header) &&
				\ !(l:header ==# 'GIT' && get(b:, 'fugitive_type', '') ==# 'blob')
		return ''
	else
		let l:ft = &filetype
		if empty(l:ft)
			let l:ft = s:empty_glyph
		endif
		let l:subtype = get(b:, 'subtype', '')
		if !empty(l:subtype)
			let l:subtype = printf('%s:', l:subtype)
		endif
		return l:subtype.l:ft
	endif
endfunction
function! LightlineLineInfo()
	let l:header = LightlineHeader()
	" Special Buffers except...
	if !empty(l:header) &&
				\ index(['DIFF'], l:header) == -1
		return LightlineLineInfoExtra(l:header)
	else
		" Equivalent to '%4l¶%-2c'
		return printf('%4d%s%-2d', line('.'), s:paragraph_glyph, col('.'))
	endif
endfunction
function! LightlineArgList()
	if !empty(LightlineHeader()) " Special Buffers
		return ''
	endif
	if argc() == 0
		return ''
	else
		" argidx is base-0
		return printf('%dA%d', argidx() + 1, argc())
	endif
endfunction
function! LightlineLineInfoExtra(...)
	if a:0 == 0
		let l:header = LightlineHeader()
	else
		let l:header = a:1
	endif
	if index(['DENITE', 'QUICKFIX', 'LOCATION LIST'], l:header) != -1
		" Equivalent to `denite#get_status('linenr')`
		return denite#get_status('linenr')
	elseif l:header ==# 'LINEDIFF'
		let l:from = b:differ.from
		let l:to   = b:differ.to
		if l:from == l:to
			return printf('L%d', b:differ.from)
		else
			return printf('L%d-%d', b:differ.from, b:differ.to)
		endif
	endif
	return ''
endfunction
function! LightlineMode()
	let l:header = LightlineHeader()
	if !empty(l:header) " Special Buffers
		return l:header
	else
		return lightline#mode() " Usual Mode
	endif
endfunction
function! LightlineModified()
	let l:header = LightlineHeader()
	" Special Buffers except...
	if !empty(l:header) &&
				\ index(['DIFF'], l:header) == -1
		" Show on:
		" - git commits
		if !((l:header ==# 'GIT' && get(b:, 'subtype', '') ==# 'commit'))
			return ''
		endif
	endif
	if !&modifiable
		return s:unmodifiable_glyph
	elseif &modified
		return s:modified_glyph
	else
		return ''
	endif
endfunction
function! LightlinePluginInfo()
	let l:header = LightlineHeader()
	if l:header ==# 'DENITE'
		" Equivalent to `denite#get_status('path')`
		return getcwd()
	elseif l:header ==# 'PYDOC'
		return printf('Type[%s]', b:pydoc_type)
	elseif l:header ==# 'UNDOTREE DIFF'
		let l:diffs = t:diffpanel.changes
		return printf('%s%d%s%d',
					\ s:glyph_sign_added, l:diffs.add,
					\ s:glyph_sign_removed, l:diffs.del
					\ )
	elseif l:header ==# 'TERMINAL'
		return fnamemodify(getcwd(), ':~:.')
	elseif index(['GIT', 'GV', 'GIT-COMMIT'], l:header) != -1
		try
			return fnamemodify(fugitive#repo().tree(), ':~')
		catch /fugitive/
			return printf('%s@%s', s:vcs_symbols['git'], fnamemodify(fugitive#repo().git_dir, ':~'))
		endtry
	elseif l:header ==# 'DIRVISH'
		let l:path = get(b:dirvish, 'lastpath', '')
		if !empty(l:path)
			return printf('<%s', fnamemodify(l:path, ':~'))
		endif
	elseif l:header ==# 'LINEDIFF'
		return b:differ.label
	elseif l:header ==# 'DIFF'
		let l:separator = ' '
		if exists('b:git_dir') && get(b:, 'fugitive_type', '') ==# 'blob'
			let l:bufname = expand('%')
			if l:bufname =~? '//0/'
				let l:string = 'Index'
			elseif l:bufname =~? '//1/'
				let l:string = 'Ancestor'
			elseif l:bufname =~? '//2/'
				let l:string = 'Target'
			elseif l:bufname =~? '//3/'
				let l:string = 'Merge'
			else
				" Show nice name for branches and tags only
				let l:commit = matchstr(FugitiveParse()[0], '^\x\+')
				let l:string = s:vimrc_lightline_fugitive_beautify(l:commit)
				let l:separator = '@'
			endif
			return printf('%s%s%s', s:vcs_symbols['git'], l:separator, l:string)
		endif
	endif
	return ''
endfunction
function! LightlinePluginInfoExtra()
	let l:header = LightlineHeader()
	if !empty(l:header) " Special Buffers
		if l:header ==# 'DENITE'
			return denite#get_status('sources')
		elseif l:header ==# 'TERMINAL'
			return '#'.b:terminal_job_pid
		elseif l:header ==# 'DADBOD'
			return printf('%s', readfile(b:db_input)[0]) "TODO: Show more than the first line
		elseif index(['QUICKFIX', 'LOCATION LIST'], l:header) != -1
			return get(w:, 'quickfix_title', '')
		elseif l:header ==# 'GV'
			return fugitive#repo().head()
		elseif l:header ==# 'GIT'
			if index(['tree', 'blob'], b:fugitive_type) != -1
				return printf('/%s', FugitivePath(@%, ''))
			elseif index(['index', 'file'], b:fugitive_type) != -1
				return fugitive#repo().head()
			elseif b:fugitive_type ==# 'commit'
				return matchstr(FugitiveParse()[0], '^\x\+')
			endif
		endif
	endif
	return ''
endfunction
function! LightlineFlags()
	return join([
				\   IsStripTrailing() ? s:strip_glyph : '',
				\ ], '')
endfunction
function! LightlineNeomake()
	let l:header = LightlineHeader()
	" Special Buffers except...
	if !empty(l:header) &&
				\ index(['DIFF'], l:header) == -1
		return ''
	endif
	" TODO: Check for available makers
	" echo map(filter(map(neomake#GetEnabledMakers(&ft), {idx, d ->
	" neomake#GetMaker(d['name'])}), "type(get(v:val, 'exe', 0)) != type('') ||
	" executable(v:val.exe)"), {idx, d -> d['name']})
	let l:options = {
				\ 'running_jobs_separator': s:working_glyph,
				\ 'format_running_job_project': '%s'.s:global_glyph,
				\ 'format_running': '{{running_job_names}}'.s:working_glyph,
				\ 'format_loclist_ok': s:good_glyph,
				\ 'format_loclist_unknown': s:ellipsis_glyph,
				\ 'format_loclist_issues': '%s',
				\ 'format_quickfix_issues:': s:global_glyph.'%s',
				\ 'format_status_enabled': s:checker_glyph.'%s',
				\ 'format_status_disabled': '%s',
				\ }
	for [l:ftype, l:string] in items(s:type_glyph)
		let l:options['format_loclist_type_'.l:ftype] = '{{count}}'.l:string
		let l:options['format_quickfix_type_'.l:ftype] = '{{count}}'.l:string
	endfor
	return neomake#statusline#get(bufnr('%'), l:options)
endfunction
function! LightlineVCS()
	if !empty(LightlineHeader()) " Special Buffers
		return ''
	endif
	let l:string = []
	" Git with Fugitive
	let l:git = fugitive#head()
	if l:git !=# ''
		let l:git = printf('%s[%s]', s:vcs_symbols['git'], l:git)
		" Detect git-svn
		if isdirectory(fnamemodify(fugitive#repo().git_dir . '/svn', ':p'))
			let l:git .= s:vcs_symbols['svn']
		endif
		call add(l:string, l:git)
	endif
	" Other stuff with Signify
	if has_key(get(b:, 'sy', {}), 'stats')
		let [l:added, l:modified, l:removed] = sy#repo#get_stats()
		let l:sy = ''
		for [l:flag, l:flagcount] in [
					\   [s:glyph_sign_added, l:added],
					\   [s:glyph_sign_removed, l:removed],
					\   [s:glyph_sign_modified, l:modified]
					\ ]
			if l:flagcount > 0
				let l:sy .= printf('%s%d', l:flag, l:flagcount)
			endif
		endfor
		if !empty(l:sy)
			let l:sy = printf('[%s]', l:sy)
			let l:sy_vcs = get(s:vcs_symbols, get(b:sy, 'updated_by', ''), '???')
			let l:sy = printf('%s%s', l:sy_vcs, l:sy)
			call add(l:string, l:sy)
		endif
	endif
	return lightline#concatenate(l:string, 0) "0 => left-hand side
endfunction
function! LightlineWordcount()
	if !empty(LightlineHeader()) " Special Buffers
		return ''
	endif
	if index(g:spelling_filetypes, &filetype) > -1
		return printf('%dW', wordcount()['words'])
	else
		return ''
	endif
endfunction
function! LightlineSpell()
	return &spell ? s:spell_glyph : ''
endfunction
function! LightlineSpellExtra()
	if &spell
		return s:spell_glyph . toupper(''.&spelllang)
	else
		return ''
	endif
endfunction

function! LightlineTablineName(n)
	let l:tname = gettabvar(a:n, 'tab_name', v:null)
	if l:tname == v:null
		" See: lightline#tab#filename
		let l:buflist = tabpagebuflist(a:n)
		let l:winnr = tabpagewinnr(a:n)
		let l:_ = expand('#'.l:buflist[l:winnr - 1].':t')
		return l:_ !=# '' ? l:_ : s:empty_glyph
	else
		return printf('%s%s', s:global_glyph, l:tname)
	endif
endfunction
function! LightlineTablineBuffers(n)
	let l:buffers = tabpagebuflist(a:n)
	let l:w_size = len(l:buffers)
	let l:b_size = len(uniq(l:buffers))
	if l:b_size > 1
		return printf('%dB', l:b_size)
	elseif l:w_size > l:b_size
		return printf('%dW%dB', l:w_size, l:b_size)
	else
		return ''
	endif
endfunction
function! LightlineTablineModified(n)
	" See: lightline#tab#modified
	" See: lightline#tab#readonly
	let l:window_number = tabpagewinnr(a:n, '$')
	let l:modified = v:false
	let l:unmodifiable = v:true
	for l:winnr in range(1, l:window_number)
		let l:modified = l:modified || gettabwinvar(a:n, l:winnr, '&modified')
		let l:unmodifiable = l:unmodifiable && !gettabwinvar(a:n, l:winnr, '&modifiable')
	endfor
	let l:string = ''
	if l:modified
		let l:string .= s:modified_glyph
	elseif l:unmodifiable
		let l:string .= s:unmodifiable_glyph
	endif
	return l:string
endfunction
"}}}
" Mode Map {{{
let g:lightline.mode_map = {
			\ 'n' : 'N',
			\ 'i' : 'I',
			\ 'R' : 'R',
			\ 'v' : 'V',
			\ 'V' : 'V-L',
			\ "\<C-v>": 'V-B',
			\ 'c' : 'C',
			\ 's' : 'S',
			\ 'S' : 'S-L',
			\ "\<C-s>": 'S-B',
			\ 't': 'T',
			\ }
"}}}
"}}}
" Highlighted Yank {{{
let g:highlightedyank_highlight_duration = 500
highlight Yank cterm=reverse gui=reverse
"}}}
" GV {{{
nnoremap <silent> <Leader>gv :GV<CR>
nnoremap <silent> <Leader>gV :GV --all<CR>
augroup vimrc_gv | autocmd!
	" Open all folds on open
	autocmd User GV FoldsOpenAll
augroup END
"}}}
" Dispatch {{{
nnoremap <silent> m<Leader>a :Make all<CR>
nnoremap <silent> m<Leader>c :Make clean<CR>
nnoremap <silent> m<Leader><CR> :Make clean<CR>:Make<CR>
"}}}
" Lexical {{{
let g:lexical#spell = 0 "Disable spelling by default...
"}}}
" Spell Loop {{{
let g:spellloop_skip_nospell = 1 " Don't loop if 'spell' is not set
let g:spell_list = ['en_gb', 'pt', 'ptao']
nmap <silent> <Leader>tS <Plug>(spellloop-toggle)
nmap <silent> <Leader>ts <Plug>(spellloop)
"}}}
" Internal || Strip Trailing {{{
nnoremap <silent> <Leader>t<Space> :ToggleStripTrailing<CR>
augroup vimrc_strip_trailing | autocmd!
	autocmd User StripTrailingState LightlineReload
augroup END
"}}}
" Dirvish |> netrw {{{
let g:loaded_netrwPlugin = 1  " Don't load netrw
" Sort folders first
" Ignore case when sorting
let g:dirvish_mode = ':sort i ,^.*[\/],'
nnoremap <silent> <Leader>b :tabnew %:h<CR>
"}}}
" Linediff {{{
let g:linediff_modify_statusline = 0
xnoremap <Leader>d :Linediff<CR>
" Shortcuts for merge conflicts. git-adjacent, not provided by Fugitive
nnoremap <silent> <Leader>gd :LinediffMerge<CR>
nnoremap <silent> <Leader>gD :LinediffPick<CR>
"}}}

" Source site specific `init.vim` {{{
" The `site` subfolder should be included in &rtp already
let $MYVIMRC_HOST = fnamemodify(stdpath('data').'/init.vim', ':p')
if filereadable($MYVIMRC_HOST)
	source $MYVIMRC_HOST
endif
"}}}

" Modeline {{{
" vim:ts=2:sw=2:noet:fdm=marker
"}}}
