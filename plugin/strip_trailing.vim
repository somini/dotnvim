" strip_trailing.vim - Simply clear trailing trash
" - trailing whitespace
" - trailing `\r`
if exists('g:loaded_strip_trailing')
	finish
endif
let g:loaded_strip_trailing = 1

function! s:StripTrailing(line1, line2)
	try
		keepjumps keeppatterns execute join([a:line1, a:line2], ',').'s@\(\s\+\|\r\)$@@'
	catch /^Vim\%((\a\+)\)\=:E486/
		echohl WarningMsg
		echomsg 'No trailing whitespace'
		echohl None
	endtry
endfunction
function! s:StripTrailing_Auto()
	if IsStripTrailing()
		normal! m`
		call s:StripTrailing('1', '$')
		keepjumps normal! ``
	endif
endfunction
function! s:ToggleStripTrailing()
	let b:strip_trailing = IsStripTrailing() ? 0 : 1
	if exists('#User#StripTrailingState')
		doautocmd <nomodeline> User StripTrailingState
	endif
endfunction
function! IsStripTrailing()
	return get(b:, 'strip_trailing', get(g:, 'strip_trailing', 0))
endfunction

command! -range=% StripTrailing call s:StripTrailing(<line1>, <line2>)
command! ToggleStripTrailing call s:ToggleStripTrailing()
augroup StripTrailing
	autocmd!
	autocmd BufWritePre * call s:StripTrailing_Auto()
augroup END
