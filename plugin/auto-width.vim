" Auto-Width: Resize window based on textwidth
" Only for buffers where 'b:auto_width_do' is set to v:true

if exists('g:loaded_auto_width')
	finish
endif
let g:loaded_auto_width = 1

function! s:AutoWidth()
	" b:auto_width_do overrrides the filetype check
	if get(b:, 'auto_width_do', '') == v:true
		if &textwidth > 20 "Bare minimum
			let l:v_size = &textwidth + 2
		else
			let l:v_size = 80
		endif
		exec 'vertical resize '.l:v_size
	endif
endfunction

" Set window width to textwidth + 2
augroup auto_width
	autocmd!
	autocmd VimResized  * call s:AutoWidth()
	autocmd BufWinEnter * call s:AutoWidth()
	autocmd BufEnter    * call s:AutoWidth()
	autocmd FileType    * call s:AutoWidth()
augroup END

" vim: ts=2 sw=2
