" Python Provider
"" Use a bat wrapper for Python Launcher
let g:python_host_prog =  fnamemodify(expand('$MYVIMRC'), ':p:h').'\bin\python2.bat'
let g:python3_host_prog = fnamemodify(expand('$MYVIMRC'), ':p:h').'\bin\python3.bat'

" Setup a sane caching directory
let $XDG_CACHE_HOME = utils#CheckDir(expand($TEMP.'/neovim'))

" Plugin: PyDoc
let g:pydoc_cmd = g:python3_host_prog . ' -m pydoc'
let g:pydoc_detect_version = 0 "Don't "detect" the version erroneously
