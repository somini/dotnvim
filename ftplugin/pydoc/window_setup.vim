" Moves the window to the top-left location
wincmd H
" Automatically resize the window vertically
let b:auto_width_do = v:true
