" Open all folds
augroup ftplugin_rst
	autocmd BufWinEnter <buffer> FoldsOpenAll
	" TODO: This is a hack to mean the future "autocmd ++once"
	autocmd BufUnload <buffer> autocmd! ftplugin_rst
augroup END
