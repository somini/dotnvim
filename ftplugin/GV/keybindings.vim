function! s:is_open()
  return getwinvar(winnr('$'), 'gv') == 1
endfunction
function! s:do_it(open, close)
  if s:is_open()
    return a:open
  else
    return a:close
  endif
endfunction
function! s:do(move)
  " Do it and refresh the commit, if it is already open
  return s:do_it(a:move.'o', a:move)
endfunction

" Movement commands for usual operations
nmap <buffer> <silent> <expr> j <SID>do(']]')
nmap <buffer> <silent> J ]]o
nmap <buffer> <silent> <expr> k <SID>do('[[')
nmap <buffer> <silent> K [[o
" l to open the commit, h to close
nmap <buffer> <silent> l o
nmap <buffer> <silent> <expr> h <SID>do_it('q', '')
" Ctrl-Movement to scroll the other window
nnoremap <buffer> <silent> <C-j> <C-w>p<C-e><C-w>p
nnoremap <buffer> <silent> <C-k> <C-w>p<C-y><C-w>p
" R: Hard-reload (fetch from remotes first)
nnoremap <buffer> R :Git fetch --all <Bar> normal r<CR>
