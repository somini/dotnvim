" Resize it to a proper size
if &textwidth == 0
	setlocal textwidth=80
endif

" Moves the window to the top-left location
wincmd H
" Automatically resize the window vertically
let b:auto_width_do = v:true

" Delete buffer on close
setlocal bufhidden=delete

" Close window on F1
nnoremap <silent> <buffer> <F1> :bdelete<CR>
