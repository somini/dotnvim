if exists('b:subtype')
	finish
endif

if &buftype ==# 'help'
	let b:subtype = 'buffer'
else
	let b:subtype = 'file'
endif

if exists('b:subtype')
	execute 'runtime! ftplugin/'.&filetype.'/'.b:subtype.'/*.vim'
endif
