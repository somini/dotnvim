" Info-specific navigation
nmap <buffer> gl <Plug>(InfoNext)
nmap <buffer> gh <Plug>(InfoPrev)
nmap <buffer> gk <Plug>(InfoUp)
nmap <buffer> gm <Plug>(InfoMenu)
