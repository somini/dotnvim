" Moves the window to the top-right location
wincmd L
" Automatically resize the window vertically
let b:auto_width_do = v:true
