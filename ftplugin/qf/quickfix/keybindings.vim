nnoremap <silent> <buffer> q    :cclose<CR>
nnoremap <silent> <buffer> <CR> <CR>
nnoremap <silent> <buffer> l <CR>:cclose<CR>
nnoremap <silent> <buffer> L <CR>:wincmd p<CR>
nnoremap <silent> <buffer> gg :crewind<CR>:wincmd p<CR>
nnoremap <silent> <buffer> j :cnext<CR>
nnoremap <silent> <buffer> J :cnext<CR>:wincmd p<CR>
nnoremap <silent> <buffer> <A-j> j
nnoremap <silent> <buffer> k :cprevious<CR>
nnoremap <silent> <buffer> K :cprevious<CR>:wincmd p<CR>
nnoremap <silent> <buffer> <A-k> k
nnoremap <silent> <buffer> G :clast<CR>:wincmd p<CR>
