nnoremap <silent> <buffer> q    :lclose<CR>
nnoremap <silent> <buffer> <CR> <CR>
nnoremap <silent> <buffer> l <CR>:lclose<CR>
nnoremap <silent> <buffer> L <CR>:wincmd p<CR>
nnoremap <silent> <buffer> gg :lrewind<CR>:wincmd p<CR>
nnoremap <silent> <buffer> j :lnext<CR>
nnoremap <silent> <buffer> J :lnext<CR>:wincmd p<CR>
nnoremap <silent> <buffer> <A-j> j
nnoremap <silent> <buffer> k :lprevious<CR>
nnoremap <silent> <buffer> K :lprevious<CR>:wincmd p<CR>
nnoremap <silent> <buffer> <A-k> k
nnoremap <silent> <buffer> G :llast<CR>:wincmd p<CR>
