" Detect if the current window is a location list
if empty(getloclist(0))
	let b:subtype = 'quickfix'
else
	let b:subtype = 'loclist'
endif

if exists('b:subtype')
	execute 'runtime! ftplugin/'.&filetype.'/'.b:subtype.'/*.vim'
endif
