" Quit if there is only a listing buffer
augroup ft_qf
	autocmd BufEnter <buffer> if winnr('$') < 2 | quit | endif
augroup END
