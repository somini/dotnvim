" Alias the movement commands for usual operations
" See https://github.com/tpope/vim-fugitive/issues/1867
nmap <buffer> <silent> gg ggj
nmap <buffer> <silent> <nowait> j )_
nmap <buffer> <silent> <nowait> k (_
"nmap <buffer> <silent> G G
" h to stage/reset
nmap <buffer> <silent> <nowait> h -
" l to open file and cleanup all windows (this might be overkill)
" L to just open it
nmap <buffer> <silent> <nowait> l <CR><C-q>
nmap <buffer> <silent> <nowait> L <CR>
" Upstream Branch manipulation
nnoremap <buffer> u :GitUpstream<CR>
