# NeoVim Dotfiles

This is my personal NeoVim configuration.

As a plugin manager, I'll use `pathogen`.
It includes plugins as git submodules, in two separate folders:

- [3rd Party](3rd-party/): Plugins taken as-is, without customisation
- [1st Party](1st-party/): My own plugins, or personal forks

There are helper scripts in the `bin` folder.

On a sliding scale of performance vs features, I'll lean towards features. Make (Neo)Vim an IDE, if you will.
All updates are manual, to avoid plugins breaking the configuration.

This should work on the latest NeoVim version, but I accept Pull Requests for
small compatibility changes.

Install [ripgrep][ripgrep] for much improved performance.
This is critical on Windows, or searches are unbearably slow.

## Installation

### Standalone Version

If you want to use this in a rush on some random machine, see Gitlab Pages: https://somini.gitlab.io/dotnvim/

There are two versions available:
- **Full**: The repository, minus detritus like README, testing directories and
  the like. This version is **good enough**.
- **Single**: The *Full* archive, where all plugins are squashed into a single
  directory. This should be the most efficient version.

These versions might have weird bugs. There's a [sanity check](.gitlab-ci.yml) on:
- Arch Linux
- CentOS 7
I also use this on some machines myself, but if it breaks, you get to keep both parts.

This also works on Windows, but it's not as thoroughly tested.

### Development Version

If you want to change the content of the repo, the easiest way is using the development version AKA cloning the repo.

This is my daily driver, so if something breaks I notice.

Clone the repository to the default NeoVim location.

```sh
git clone --recursive "https://gitlab.com/somini/dotnvim" "$HOME/.config/nvim"
"$HOME/.config/nvim/bin/nvim-reload"
make -C "$HOME/.config/nvim/spell"
```

## Screenshots

TBA


[ripgrep]: https://github.com/BurntSushi/ripgrep
