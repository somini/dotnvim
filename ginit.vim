if exists('g:GtkGuiLoaded')  " Neovim-GTK
	call rpcnotify(1, 'Gui', 'Option', 'Tabline', 0) " Disable external tabline
endif

if exists('g:GuiLoaded') " Loaded the *nvim-gui-shim*
	" Disable the tabline
	GuiTabline 0
	" Disable the popup menu
	GuiPopupmenu 0

	" Font Configuration
	function! GuiFontParse()
		let matches = matchlist(g:GuiFont, '\v([^:]+):h([0-9]+(\.[0-9]+)?)(:([bli]))?')
		return {'font': matches[1], 'size': str2float(matches[2]), 'attrs': matches[4]}
	endfunction

	function! GuiFontBuild(value)
		return printf('%s:h%f:%s', a:value.font, a:value.size, a:value.attrs)
	endfunction

	function! GuiFontSize(nsize)
		let l:v = GuiFontParse()
		let l:v.size = a:nsize
		execute 'GuiFont!' GuiFontBuild(l:v)
	endfunction

	function! GuiFontSizeDelta(delta)
		let l:v = GuiFontParse()
		let l:v.size += a:delta
		execute 'GuiFont!' GuiFontBuild(l:v)
	endfunction

	nnoremap <silent> <C-ScrollWheelUp>   :call GuiFontSizeDelta(+1)<CR>
	nnoremap <silent> <C-ScrollWheelDown> :call GuiFontSizeDelta(-1)<CR>
	nnoremap <silent> <C-+> :call GuiFontSizeDelta(+1)<CR>
	nnoremap <silent> <C--> :call GuiFontSizeDelta(-1)<CR>
endif

" Source site specific `ginit.vim` {{{
" The `site` subfolder should be included in &rtp already
let $MYGVIMRC_HOST = fnamemodify(stdpath('data').'/ginit.vim', ':p')
if filereadable($MYGVIMRC_HOST)
	source $MYGVIMRC_HOST
endif
"}}}
